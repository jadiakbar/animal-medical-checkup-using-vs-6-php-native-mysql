VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Entri_Barang_Detail 
   Caption         =   "Data Barang Detil"
   ClientHeight    =   7650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10530
   LinkTopic       =   "Form1"
   ScaleHeight     =   7650
   ScaleWidth      =   10530
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Caption         =   "Filter"
      Height          =   975
      Left            =   0
      TabIndex        =   14
      Top             =   2880
      Width           =   9135
      Begin VB.CommandButton Cmdkeluar 
         Caption         =   "&Keluar"
         DragIcon        =   "barang_detail.frx":0000
         Height          =   615
         Left            =   7920
         Picture         =   "barang_detail.frx":0B02
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Cmdberikut 
         DownPicture     =   "barang_detail.frx":1604
         Height          =   495
         Left            =   1680
         Picture         =   "barang_detail.frx":2006
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdterakhir 
         Height          =   495
         Left            =   2400
         Picture         =   "barang_detail.frx":2A08
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdpertama 
         Height          =   495
         Left            =   240
         Picture         =   "barang_detail.frx":340A
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdsebelum 
         Height          =   495
         Left            =   960
         Picture         =   "barang_detail.frx":3E0C
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   9120
      TabIndex        =   8
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Cmdbatal 
         Caption         =   "&Batal"
         Height          =   615
         Left            =   120
         Picture         =   "barang_detail.frx":480E
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton Cmdtambah 
         Caption         =   "&Tambah"
         Height          =   615
         Left            =   120
         Picture         =   "barang_detail.frx":5210
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Cmdsimpan 
         Caption         =   "&Simpan"
         Height          =   615
         Left            =   120
         Picture         =   "barang_detail.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton Cmdedit 
         Caption         =   "&Edit"
         Height          =   615
         Left            =   120
         Picture         =   "barang_detail.frx":6614
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton Cmdhapus 
         Caption         =   "&Hapus"
         Height          =   615
         Left            =   120
         Picture         =   "barang_detail.frx":7016
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   3120
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Entri Barang Detil"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      Begin VB.TextBox txt_uraian 
         Height          =   735
         Left            =   2400
         TabIndex        =   22
         Top             =   1920
         Width           =   3615
      End
      Begin VB.TextBox txt_harga_sat 
         Height          =   375
         Left            =   2400
         TabIndex        =   4
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox txt_nama_brg 
         Height          =   375
         Left            =   2400
         TabIndex        =   3
         Top             =   840
         Width           =   1935
      End
      Begin VB.TextBox txt_jenis_hewan 
         Height          =   375
         Left            =   2400
         TabIndex        =   2
         Top             =   360
         Width           =   2775
      End
      Begin VB.TextBox txt_id_brg 
         Enabled         =   0   'False
         Height          =   375
         Left            =   6960
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   360
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Uraian"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Harga Satuan"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Jenis Hewan"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Nama Barang"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   960
         Width           =   1575
      End
   End
   Begin MSAdodcLib.Adodc Ado_brg_detail 
      Height          =   375
      Left            =   0
      Top             =   7080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "barang_detail"
      Caption         =   "Data Barang Detail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "barang_detail.frx":7A18
      Height          =   3615
      Left            =   0
      TabIndex        =   20
      Top             =   3960
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   6376
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Entri_Barang_Detail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Baru As Boolean

Private Sub Cmdbatal_Click()
Tombol True, True, False, False, True
Ado_brg_detail.Recordset.Cancel
Kosong
End Sub

Private Sub Cmdberikut_Click()
'Menuju ke record berikutnya
Ado_brg_detail.Recordset.MoveNext
'Jika berada di record terakhir menuju ke record terakhir
If Ado_brg_detail.Recordset.EOF Then
Ado_brg_detail.Recordset.MoveLast
End If
End Sub

Private Sub Cmdedit_Click()
Tombol False, False, True, True, False
With Ado_brg_detail.Recordset
txt_jenis_hewan = !jenis_hewan
txt_nama_brg = !nama_brg
txt_harga_sat = !harga_sat
txt_uraian = !uraian
End With
Baru = False
End Sub

Private Sub Cmdhapus_Click()
Dim hapus
hapus = MsgBox("Apakah anda yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus Data")
If hapus = vbYes Then
    Ado_brg_detail.Recordset.Delete
    Ado_brg_detail.Recordset.MoveLast
Else
    MsgBox "Data tidak jadi dihapus!", vbOKOnly + vbInformation, "Batal menghapus"
End If
End Sub

Private Sub Cmdkeluar_Click()
Unload Me
End Sub

Private Sub Cmdpertama_Click()
'Menuju ke record pertama
Ado_brg_detail.Recordset.MoveFirst
End Sub

Private Sub Cmdsebelum_Click()
'Menuju ke record sebelumnya
Ado_brg_detail.Recordset.MovePrevious
'Jika berada di record pertama menuju ke record pertama
If Ado_brg_detail.Recordset.BOF Then
Ado_brg_detail.Recordset.MoveFirst
End If
End Sub

Private Sub Cmdsimpan_Click()
Tombol True, True, False, False, True
With Ado_brg_detail.Recordset
If Baru Then .AddNew
!jenis_hewan = txt_jenis_hewan.Text
!nama_brg = txt_nama_brg.Text
!harga_sat = txt_harga_sat.Text
!uraian = txt_uraian.Text
.Update
.Sort = "id_brg"
End With
Kosong
End Sub

Private Sub Cmdtambah_Click()
Tombol False, False, True, True, False
Baru = True
Kosong
End Sub

Private Sub Cmdterakhir_Click()
Ado_brg_detail.Recordset.MoveLast
End Sub

Private Sub Form_Load()
Ado_brg_detail.ConnectionString = "DSN=si_serkes_hewan"
Ado_brg_detail.RecordSource = "barang_detail"
Ado_brg_detail.Refresh
Ado_brg_detail.Recordset.Sort = "id_brg"
Baru = False
End Sub

Public Sub Tombol(tambah, edit, simpan, batal, hapus As Boolean)
Cmdtambah.Enabled = tambah
Cmdedit.Enabled = edit
Cmdsimpan.Enabled = simpan
Cmdbatal.Enabled = batal
Cmdhapus.Enabled = hapus
End Sub

Public Sub Kosong()
txt_jenis_hewan.Text = ""
txt_nama_brg.Text = ""
txt_harga_sat.Text = ""
txt_uraian.Text = ""
End Sub
