VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Entri_Barang 
   Caption         =   "Data Hewan"
   ClientHeight    =   7650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10530
   LinkTopic       =   "Form2"
   ScaleHeight     =   7650
   ScaleWidth      =   10530
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Entri Hewan"
      Height          =   2895
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   9135
      Begin VB.TextBox txt_id_brg 
         Enabled         =   0   'False
         Height          =   375
         Left            =   7080
         TabIndex        =   22
         Top             =   600
         Visible         =   0   'False
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker txt_tgl_barang 
         Bindings        =   "barang.frx":0000
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "yyyy-MM-dd"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   375
         Left            =   2400
         TabIndex        =   21
         Top             =   2040
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "yyyy-mm-dd"
         Format          =   16252931
         CurrentDate     =   41886
      End
      Begin MSDataListLib.DataCombo txt_jenis_hewan 
         Bindings        =   "barang.frx":0028
         DataField       =   "jenis_hewan"
         DataSource      =   "Ado_jenis_hewan"
         Height          =   315
         Left            =   2400
         TabIndex        =   20
         Top             =   1080
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   556
         _Version        =   393216
         ListField       =   "jenis_hewan"
         BoundColumn     =   "jenis_hewan"
         Text            =   ""
      End
      Begin VB.TextBox txt_id_pemohon 
         Height          =   375
         Left            =   2400
         TabIndex        =   15
         Top             =   600
         Width           =   2175
      End
      Begin VB.TextBox txt_jumlah 
         Height          =   375
         Left            =   2400
         TabIndex        =   14
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "Jenis Hewan"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Identitas Pemohon"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Jumlah"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label Label4 
         Caption         =   "Tanggal Pengiriman Hewan"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   2160
         Width           =   2055
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   9120
      TabIndex        =   7
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Cmdhapus 
         Caption         =   "&Hapus"
         Height          =   615
         Left            =   120
         Picture         =   "barang.frx":006B
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   3120
         Width           =   975
      End
      Begin VB.CommandButton Cmdedit 
         Caption         =   "&Edit"
         Height          =   615
         Left            =   120
         Picture         =   "barang.frx":0A6D
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton Cmdsimpan 
         Caption         =   "&Simpan"
         Height          =   615
         Left            =   120
         Picture         =   "barang.frx":146F
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton Cmdtambah 
         Caption         =   "&Tambah"
         Height          =   615
         Left            =   120
         Picture         =   "barang.frx":1E71
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Cmdbatal 
         Caption         =   "&Batal"
         Height          =   615
         Left            =   120
         Picture         =   "barang.frx":2873
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   2400
         Width           =   975
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filter"
      Height          =   975
      Left            =   0
      TabIndex        =   1
      Top             =   2880
      Width           =   9135
      Begin MSAdodcLib.Adodc Ado_jenis_hewan 
         Height          =   330
         Left            =   4560
         Top             =   360
         Visible         =   0   'False
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   2
         LockType        =   3
         CommandType     =   1
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
         OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   "select jenis_hewan from barang_detail order by jenis_hewan asc"
         Caption         =   "Jenis Hewan"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin VB.CommandButton Cmdsebelum 
         Height          =   495
         Left            =   960
         Picture         =   "barang.frx":3275
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdpertama 
         Height          =   495
         Left            =   240
         Picture         =   "barang.frx":3C77
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdterakhir 
         Height          =   495
         Left            =   2400
         Picture         =   "barang.frx":4679
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdberikut 
         DownPicture     =   "barang.frx":507B
         Height          =   495
         Left            =   1680
         Picture         =   "barang.frx":5A7D
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdkeluar 
         Caption         =   "&Keluar"
         DragIcon        =   "barang.frx":647F
         Height          =   615
         Left            =   7920
         Picture         =   "barang.frx":6F81
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
   End
   Begin MSAdodcLib.Adodc Ado_hewan 
      Height          =   375
      Left            =   0
      Top             =   7080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "barang"
      Caption         =   "Data Hewan"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "barang.frx":7A83
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   3960
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   6376
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Entri_Barang"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Baru As Boolean



Private Sub Cmdbatal_Click()
Tombol True, True, False, False, True
Ado_hewan.Recordset.Cancel
Kosong
End Sub

Private Sub Cmdberikut_Click()
Ado_hewan.Recordset.MoveNext
If Ado_hewan.Recordset.EOF Then
Ado_hewan.Recordset.MoveLast
End If
End Sub

Private Sub Cmdedit_Click()
Tombol False, False, True, True, False
With Ado_hewan.Recordset
txt_id_pemohon = !id_pemohon
txt_jenis_hewan = !jenis_hewan
txt_jumlah = !jumlah
txt_tgl_barang = !tgl_barang
End With

Baru = False
End Sub

Private Sub Cmdhapus_Click()
Dim hapus
hapus = MsgBox("Apakah anda yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus Data")
If hapus = vbYes Then
    Ado_hewan.Recordset.Delete
    Ado_hewan.Recordset.MoveLast
Else
    MsgBox "Data tidak jadi dihapus!", vbOKOnly + vbInformation, "Batal menghapus"
End If
End Sub

Private Sub Cmdkeluar_Click()
Unload Me
End Sub

Private Sub Cmdpertama_Click()
Ado_hewan.Recordset.MoveFirst
End Sub

Private Sub Cmdsebelum_Click()
Ado_hewan.Recordset.MovePrevious
If Ado_hewan.Recordset.BOF Then
Ado_hewan.Recordset.MoveFirst
End If
End Sub

Private Sub Cmdsimpan_Click()
Tombol True, True, False, False, True
With Ado_hewan.Recordset
If Baru Then .AddNew
!id_pemohon = txt_id_pemohon.Text
!jenis_hewan = txt_jenis_hewan.Text
!jumlah = txt_jumlah.Text
!tgl_barang = txt_tgl_barang.Value
.Update
.Sort = "id_brg"
End With
Kosong
End Sub

Private Sub Cmdtambah_Click()
Tombol False, False, True, True, False
Baru = True
Kosong
End Sub

Private Sub Cmdterakhir_Click()
Ado_hewan.Recordset.MoveLast
End Sub

Private Sub Form_Load()
Ado_hewan.ConnectionString = "DSN=si_serkes_hewan"
Ado_hewan.RecordSource = "barang"
Ado_hewan.Refresh
Ado_hewan.Recordset.Sort = "id_brg"
Baru = False
End Sub

Public Sub Tombol(tambah, edit, simpan, batal, hapus As Boolean)
Cmdtambah.Enabled = tambah
Cmdedit.Enabled = edit
Cmdsimpan.Enabled = simpan
Cmdbatal.Enabled = batal
Cmdhapus.Enabled = hapus
End Sub

Public Sub Kosong()
txt_id_pemohon.Text = ""
txt_jenis_hewan.Text = ""
txt_jumlah.Text = ""
End Sub
