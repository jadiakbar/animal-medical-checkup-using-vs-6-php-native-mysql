VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Menu_Sertifikasi 
   BackColor       =   &H80000001&
   Caption         =   "Menu Sertifikat Hewan"
   ClientHeight    =   8745
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   10800
   ScaleHeight     =   583
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   720
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4800
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Serkes_Hewan.frx":0000
            Key             =   "Surat Penugasan"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Serkes_Hewan.frx":0A12
            Key             =   "Sertifikasi Kesehatan"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Serkes_Hewan.frx":1424
            Key             =   "Kwitansi"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Serkes_Hewan.frx":1E36
            Key             =   "Report Semua Kegiatan"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10800
      _ExtentX        =   19050
      _ExtentY        =   1111
      ButtonWidth     =   2752
      ButtonHeight    =   953
      Appearance      =   1
      ImageList       =   "ImageList1"
      DisabledImageList=   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Permohonan"
            Key             =   "Permohonan"
            Description     =   "Permohonan"
            Object.ToolTipText     =   "Surat Permohonan Sertifikat Hewan"
            ImageIndex      =   1
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Surat Tugas"
            Key             =   "Surat Tugas"
            Description     =   "Surat Tugas"
            Object.ToolTipText     =   "Surat Penugasan Sertifikat Hewan"
            ImageIndex      =   2
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Sertifikat Kesehatan"
            Description     =   "Sertifikat Kesehatan"
            Object.ToolTipText     =   "Sertifikat Kesehatan Hewan"
            ImageIndex      =   4
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Kwitansi"
            Key             =   "Kwitansi"
            Description     =   "Kwitansi"
            Object.ToolTipText     =   "Kwitansi Sertifikat Kesehatan Hewan"
            ImageIndex      =   3
            MixedState      =   -1  'True
         EndProperty
      EndProperty
      BorderStyle     =   1
      MouseIcon       =   "Serkes_Hewan.frx":2848
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   7500
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   96375
      ExtentX         =   169995
      ExtentY         =   13229
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin VB.Menu Menu_Pemohon 
      Caption         =   "&Pemohon"
   End
   Begin VB.Menu Menu_Data_Hewan 
      Caption         =   "&Hewan"
   End
   Begin VB.Menu Menu_Surat_Tugas 
      Caption         =   "&Surat Tugas"
   End
   Begin VB.Menu Menu_Sertifikat 
      Caption         =   "&Sertifikat"
   End
   Begin VB.Menu Menu_Kwitansi 
      Caption         =   "&Kwitansi"
   End
   Begin VB.Menu Menu_Barang_Detail 
      Caption         =   "&Barang Detail"
   End
   Begin VB.Menu Keluar 
      Caption         =   "&Exit"
   End
End
Attribute VB_Name = "Menu_Sertifikasi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Keluar_Click()
End
End Sub

Private Sub Menu_Barang_Detail_Click()
Entri_Barang_Detail.Show
End Sub

Private Sub Menu_Data_Hewan_Click()
Entri_Barang.Show
End Sub

Private Sub Menu_Kwitansi_Click()
Entri_Kwitansi.Show
End Sub

Private Sub Menu_Pemohon_Click()
Entri_Pemohon.Show
End Sub

Private Sub Menu_Sertifikat_Click()
Entri_Sertifikat.Show
End Sub

Private Sub Menu_Surat_Tugas_Click()
Entri_Surat_Tugas.Show
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Index
        Case 1: WebBrowser1.navigate ("http://localhost/si_serkes_hewan/surat_penugasan.php")
        Case 2: WebBrowser1.navigate ("http://localhost/si_serkes_hewan/kh1.php")
        Case 4: WebBrowser1.navigate ("http://localhost/si_serkes_hewan/kwitansi.php")
        Case 3: WebBrowser1.navigate ("http://localhost/si_serkes_hewan/sertifikat_kesehatan.php")
    End Select
End Sub

Private Sub Form_Load()
WebBrowser1.navigate ("http://localhost/si_serkes_hewan/surat_penugasan.php")
End Sub

