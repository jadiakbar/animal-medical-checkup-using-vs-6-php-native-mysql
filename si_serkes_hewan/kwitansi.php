<?php require_once('Connections/si_serkes_hewan.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_kwi = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_kwi = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_kwi = sprintf("SELECT barang_detail.uraian, barang.jumlah, barang_detail.harga_sat, (barang.jumlah*barang_detail.harga_sat) Pungutan, barang.jenis_hewan FROM barang_detail, barang, pemohon WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.jenis_hewan=barang_detail.jenis_hewan AND pemohon.id_pemohon=%s", GetSQLValueString($colname_kwi, "text"));
$kwi = mysql_query($query_kwi, $si_serkes_hewan) or die(mysql_error());
$row_kwi = mysql_fetch_assoc($kwi);
$totalRows_kwi = mysql_num_rows($kwi);

$colname_total = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_total = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_total = sprintf("SELECT SUM(barang.jumlah*barang_detail.harga_sat) AS Tot_Sub FROM pemohon, barang, barang_detail WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.jenis_hewan=barang_detail.jenis_hewan AND pemohon.id_pemohon=%s", GetSQLValueString($colname_total, "text"));
$total = mysql_query($query_total, $si_serkes_hewan) or die(mysql_error());
$row_total = mysql_fetch_assoc($total);
$totalRows_total = mysql_num_rows($total);

$colname_no_kwi = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_no_kwi = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_no_kwi = sprintf("SELECT * FROM pemohon, kwitansi, sertifikat WHERE pemohon.id_pemohon=kwitansi.id_pemohon AND pemohon.id_pemohon=sertifikat.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_no_kwi, "text"));
$no_kwi = mysql_query($query_no_kwi, $si_serkes_hewan) or die(mysql_error());
$row_no_kwi = mysql_fetch_assoc($no_kwi);
$totalRows_no_kwi = mysql_num_rows($no_kwi);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_bndh = "SELECT * FROM bendaharawan";
$bndh = mysql_query($query_bndh, $si_serkes_hewan) or die(mysql_error());
$row_bndh = mysql_fetch_assoc($bndh);
$totalRows_bndh = mysql_num_rows($bndh);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_dokter = "SELECT * FROM nama_dokter";
$dokter = mysql_query($query_dokter, $si_serkes_hewan) or die(mysql_error());
$row_dokter = mysql_fetch_assoc($dokter);
$totalRows_dokter = mysql_num_rows($dokter);

$colname_tgl_kwit = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_kwit = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_kwit = sprintf("SELECT DATE_FORMAT(kwitansi.tgl_kwitansi, '%%d %%M %%Y') AS Tgl_Kwi FROM kwitansi WHERE kwitansi.id_pemohon=%s", GetSQLValueString($colname_tgl_kwit, "text"));
$tgl_kwit = mysql_query($query_tgl_kwit, $si_serkes_hewan) or die(mysql_error());
$row_tgl_kwit = mysql_fetch_assoc($tgl_kwit);
$totalRows_tgl_kwit = mysql_num_rows($tgl_kwit);

$i = 1; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.font1 {
	font-size: 34px;
}
.font2 {
	font-size: 30px;
	color: #03F;
}
.font3 {
	font-size: 22px;
	color: #03F;
}
.font_isi_bawah {
	font-size: 20px;
	color: #03F;
}
.font_judul {
	font-size: 24px;
	color: #03F;
}
</style>
</head>
<body>
<form id="form1" name="form1" method="get" action="kwitansi.php">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><div align="center">
        <table width="1000" border="2" cellspacing="0" cellpadding="0" bgcolor="#D4D0C8">
          <tr>
            <td>ID Pemohon :
              <label for="id_pemohon5"></label>
              <label for="id_pemohon6"></label>
              <input type="text" name="id_pemohon" id="id_pemohon6" />
              <input type="submit" name="show" id="show" value="Preview" />
              <input name="close" type="button" id="close" onclick="window.print()" value="Print" /></td>
            </tr>
        </table>
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="30">.</td>
            <td width="584">&nbsp;</td>
            <td width="137">&nbsp;</td>
            <td width="249">&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="left"><img src="gambar/600px-Kementerian_Pertanian_Republik_Indonesia.svg.png" width="200" height="193" /></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="left"><span class="font3">KEMENTRIAN PERTANIAN</span></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="left"><span class="font3">BADAN KARANTINA PERTANIAN</span></div></td>
            <td class="font_isi_bawah"><div align="left">No. Kuitansi</div></td>
            <td class="font_isi_bawah"><div align="left">: <?php echo $row_no_kwi['no_kwitansi']; ?></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><div align="left"><span class="font3">BALAI KARANTINA PERTANIAN TEMBILAHAN</span></div></td>
            <td class="font_isi_bawah"><div align="left">No. HC</div></td>
            <td class="font_isi_bawah"><div align="left">: <?php echo $row_no_kwi['no_sertifikat']; ?></div></td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
          </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td><div align="center">
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="7"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="7" class="font_judul">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="7" class="font_judul"><div align="center" class="font2"><strong>TANDA BUKTI PEMBAYARAN</strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="30">.</td>
            <td colspan="6" class="font_judul"><div align="center"></div></td>
            <td width="56" class="font_judul"><div align="center"></div></td>
            <td width="29">&nbsp;</td>
            </tr>
          <tr>
            <td>.</td>
            <td colspan="6" class="font_judul">&nbsp;</td>
            <td class="font_judul">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">Sudah terima dari</div></td>
            <td colspan="5" class="font_isi_bawah"><div align="left">: <?php echo $row_no_kwi['nm_pemohon']; ?></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">Banyaknya uang</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: Rp. <?php echo $row_total['Tot_Sub']; ?>.-</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">Untuk pembayaran</div></td>
            <td colspan="5" class="font_isi_bawah"><div align="left">: Penerimaan Negara Bukan Pajar (PNBP) sesuai Peraturan Pemerintah Republik Indonesia
            </div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td width="55" class="font_isi_bawah"><div align="left"></div></td>
            <td width="142" class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="5" class="font_isi_bawah"><div align="left">: No. 7 Tahun 2004 Tanggal 11 Februari 2004 dengan perincian sebagai berikut :</div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td colspan="9"><div align="left">.</div></td>
            </tr>
          <tr>
            <td colspan="9"><div align="left">.</div></td>
          </tr>
          <tr>
            <td colspan="9"><div align="left">.</div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi_bawah"><div align="center">
              <table width="900" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="30"><div align="center">No</div></td>
                  <td width="200"><div align="center">JENIS HEWAN</div></td>
                  <td width="247"><div align="center">URAIAN JENIS PENERIMAAN</div></td>
                  <td width="125"><div align="center">JUMLAH</div></td>
                  <td width="130"><div align="center">TARIF/ SATUAN</div></td>
                  <td width="130"><div align="center">JML PUNGUTAN</div></td>
                </tr>
              </table>
            </div>              <div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi_bawah"><div align="center">
              <table width="900" border="1" cellpadding="2" cellspacing="0">
                <?php do { ?>
                  <tr>
                    <td width="30"><div align="center"><?php echo $i; ?></div></td>
                    <td width="200"><div align="left"><?php echo $row_kwi['jenis_hewan']; ?></div></td>
                    <td width="247"><div align="left"><?php echo $row_kwi['uraian']; ?></div></td>
                    <td width="125"><div align="center"><?php echo $row_kwi['jumlah']; ?> Ekor</div></td>
                    <td width="130"><div align="right">Rp. <?php echo $row_kwi['harga_sat']; ?>.-</div></td>
                    <td width="130"><div align="right">Rp. <?php echo $row_kwi['Pungutan']; ?>.-</div></td>
                  </tr> <?php $i++ ?>
                  <?php } while ($row_kwi = mysql_fetch_assoc($kwi)); ?>
              </table>
            </div>              <div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="7" class="font_isi_bawah"><div align="center">
              <table width="900" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td width="622">JUMLAH</td>
                  <td width="264"><div align="right">Rp. <?php echo $row_total['Tot_Sub']; ?>.-</div></td>
                </tr>
              </table>
            </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td width="198" class="font_isi_bawah">&nbsp;</td>
            <td width="220" class="font_isi_bawah">&nbsp;</td>
            <td width="220" class="font_isi_bawah">&nbsp;</td>
            <td width="50" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Mengetahui / Menyetujui</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Kepala Balai / Atasan Langsung</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Tembilahan, <?php echo $row_tgl_kwit['Tgl_Kwi']; ?></div>              <div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Bendaharawan / Pembantu Bendaharawan</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center"><u><?php echo $row_dokter['nama']; ?></u></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center"><u><?php echo $row_bndh['nama']; ?></u></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Nip. <?php echo $row_dokter['nip']; ?></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Nip. <?php echo $row_bndh['nip']; ?></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($kwi);

mysql_free_result($total);

mysql_free_result($no_kwi);

mysql_free_result($bndh);

mysql_free_result($dokter);

mysql_free_result($tgl_kwit);
?>
