-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2014 at 02:24 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `si_serkes_hewan`
--
CREATE DATABASE IF NOT EXISTS `si_serkes_hewan` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `si_serkes_hewan`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id_brg` int(5) NOT NULL AUTO_INCREMENT,
  `id_pemohon` varchar(20) NOT NULL,
  `jenis_hewan` varchar(25) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `tgl_barang` date NOT NULL,
  PRIMARY KEY (`id_brg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_brg`, `id_pemohon`, `jenis_hewan`, `jumlah`, `tgl_barang`) VALUES
(11, '170', 'Sapi Bali Jantan', 2, '2014-09-25'),
(12, '170', 'Sapi Bali Betina', 11, '2014-09-25'),
(13, '181', 'Kambing Jantan', 10, '2014-09-26'),
(14, '181', 'Kerbau Jantan', 2, '2014-09-26');

-- --------------------------------------------------------

--
-- Table structure for table `barang_detail`
--

CREATE TABLE IF NOT EXISTS `barang_detail` (
  `id_brg` int(5) NOT NULL AUTO_INCREMENT,
  `jenis_hewan` varchar(25) NOT NULL,
  `nama_brg` varchar(25) NOT NULL,
  `harga_sat` int(10) NOT NULL,
  `uraian` text NOT NULL,
  PRIMARY KEY (`id_brg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `barang_detail`
--

INSERT INTO `barang_detail` (`id_brg`, `jenis_hewan`, `nama_brg`, `harga_sat`, `uraian`) VALUES
(1, 'Sapi Betina', 'Sapi', 25000, 'Sapi Ternak'),
(2, 'Sapi Jantan', 'Sapi', 25000, 'Sapi Ternak'),
(3, 'Kambing Jantan', 'Kambing', 17000, 'Kambing Ternak'),
(4, 'Kambing Betina', 'Kambing', 20000, 'Kambing Ternak'),
(5, 'Kerbau Jantan', 'Kerbau', 35000, 'Kerbau Ternak'),
(6, 'Kerbau Betina', 'Kerbau', 38000, 'Kerbau Ternak'),
(8, 'Sapi Bali Betina', 'Sapi', 35000, 'Sapi Bali Betina Ternak'),
(10, 'Sapi Bali Jantan', 'Sapi', 32000, 'Sapi Bali Jantan Ternak');

-- --------------------------------------------------------

--
-- Table structure for table `bendaharawan`
--

CREATE TABLE IF NOT EXISTS `bendaharawan` (
  `id_bnd` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  PRIMARY KEY (`id_bnd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bendaharawan`
--

INSERT INTO `bendaharawan` (`id_bnd`, `nama`, `nip`) VALUES
(1, 'Drh. Rosdiani', '198306112009122002');

-- --------------------------------------------------------

--
-- Table structure for table `bulan`
--

CREATE TABLE IF NOT EXISTS `bulan` (
  `id_bul` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(5) NOT NULL,
  PRIMARY KEY (`id_bul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `bulan`
--

INSERT INTO `bulan` (`id_bul`, `bulan`) VALUES
(1, '01'),
(2, '02'),
(3, '03'),
(4, '04'),
(5, '05'),
(6, '06'),
(7, '07'),
(8, '08'),
(9, '09'),
(10, '10'),
(11, '11'),
(12, '12');

-- --------------------------------------------------------

--
-- Table structure for table `kepala_bkp`
--

CREATE TABLE IF NOT EXISTS `kepala_bkp` (
  `id_kpl` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `nip` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kpl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kepala_bkp`
--

INSERT INTO `kepala_bkp` (`id_kpl`, `nama`, `keterangan`, `nip`) VALUES
(1, 'Ir. Holland Tambunan, M.MA', 'BKP. Kelas 1 Pekanbaru', '196812241994031002');

-- --------------------------------------------------------

--
-- Table structure for table `kwitansi`
--

CREATE TABLE IF NOT EXISTS `kwitansi` (
  `id_kwi` int(5) NOT NULL AUTO_INCREMENT,
  `no_kwitansi` varchar(30) NOT NULL,
  `id_pemohon` varchar(20) NOT NULL,
  `tgl_kwitansi` date NOT NULL,
  PRIMARY KEY (`id_kwi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `kwitansi`
--

INSERT INTO `kwitansi` (`id_kwi`, `no_kwitansi`, `id_pemohon`, `tgl_kwitansi`) VALUES
(5, '000035/TKB/III/2014', '170', '2014-09-24'),
(6, '000036/TKB/III/2014', '181', '2014-09-24');

-- --------------------------------------------------------

--
-- Table structure for table `nama_dokter`
--

CREATE TABLE IF NOT EXISTS `nama_dokter` (
  `id_dok` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  PRIMARY KEY (`id_dok`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `nama_dokter`
--

INSERT INTO `nama_dokter` (`id_dok`, `nama`, `nip`) VALUES
(1, 'Drh. Iswandi Bachtiar', '197506052006041006');

-- --------------------------------------------------------

--
-- Table structure for table `pemohon`
--

CREATE TABLE IF NOT EXISTS `pemohon` (
  `id_pemohon` varchar(20) NOT NULL,
  `nm_pemohon` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `tgl_daftar` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pemohon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemohon`
--

INSERT INTO `pemohon` (`id_pemohon`, `nm_pemohon`, `alamat`, `no_telp`, `tgl_daftar`) VALUES
('170', 'Sri Subekti', 'JL. Geriliya, No 25', '08123692254', '2014-09-02 20:48:05'),
('181', 'H. Rahmat', 'Jl. M. Boya, No.25, Lr. Nangka', '085278898965', '2014-09-02 21:20:44');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE IF NOT EXISTS `sertifikat` (
  `id_sert` int(5) NOT NULL AUTO_INCREMENT,
  `no_sertifikat` varchar(30) NOT NULL,
  `no_pengesahan` varchar(30) NOT NULL,
  `id_pemohon` varchar(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tgl_dikeluarkan` date NOT NULL,
  PRIMARY KEY (`id_sert`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sertifikat`
--

INSERT INTO `sertifikat` (`id_sert`, `no_sertifikat`, `no_pengesahan`, `id_pemohon`, `keterangan`, `tgl_dikeluarkan`) VALUES
(7, '2014.1.008.10.24.K.000231', '0640528', '170', 'Sertifikat Sanitasi Produk Hewan', '2014-09-24'),
(8, '2014.1.008.10.24.K.000233', '0640529', '181', 'Sertifikat Sanitasi Produk Hewan', '2014-09-24');

-- --------------------------------------------------------

--
-- Table structure for table `surat_tugas`
--

CREATE TABLE IF NOT EXISTS `surat_tugas` (
  `id_srt` int(11) NOT NULL AUTO_INCREMENT,
  `no_srt` varchar(40) NOT NULL,
  `id_pemohon` varchar(20) NOT NULL,
  `daerah_asal` varchar(25) NOT NULL,
  `daerah_tujuan` varchar(25) NOT NULL,
  `pelabuhan_muat` varchar(25) NOT NULL,
  `pelabuhan_bongkar` varchar(25) NOT NULL,
  `alat_angkut` varchar(25) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tgl_surat_tgs` date NOT NULL,
  PRIMARY KEY (`id_srt`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `surat_tugas`
--

INSERT INTO `surat_tugas` (`id_srt`, `no_srt`, `id_pemohon`, `daerah_asal`, `daerah_tujuan`, `pelabuhan_muat`, `pelabuhan_bongkar`, `alat_angkut`, `keterangan`, `tgl_surat_tgs`) VALUES
(5, '2014.1.008.10.24.K.000231', '181', 'Tembilahan', 'Tanjung Pinang', 'Parit 21', 'Cempaka 2', 'Motor Laut', 'HEWAN SEHAT DAN LAYAK DIBERANGKATKAN', '2014-09-25'),
(6, '2014.1.008.10.24.K.000233', '170', 'Tembilahan', 'Batam', 'Parit 13', 'Palung 9', 'Kapal Motor', 'HEWAN SEHAT DAN LAYAK DIBERANGKATKAN', '2014-09-26');

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE IF NOT EXISTS `tahun` (
  `id_thn` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(5) NOT NULL,
  PRIMARY KEY (`id_thn`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`id_thn`, `tahun`) VALUES
(1, '2014'),
(2, '2015'),
(3, '2016'),
(4, '2017'),
(5, '2018'),
(6, '2019'),
(7, '2020');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
