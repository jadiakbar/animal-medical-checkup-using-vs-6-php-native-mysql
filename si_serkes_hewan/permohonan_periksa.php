<?php require_once('Connections/si_serkes_hewan.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_bkp = "SELECT * FROM kepala_bkp";
$bkp = mysql_query($query_bkp, $si_serkes_hewan) or die(mysql_error());
$row_bkp = mysql_fetch_assoc($bkp);
$totalRows_bkp = mysql_num_rows($bkp);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_dokter = "SELECT * FROM nama_dokter";
$dokter = mysql_query($query_dokter, $si_serkes_hewan) or die(mysql_error());
$row_dokter = mysql_fetch_assoc($dokter);
$totalRows_dokter = mysql_num_rows($dokter);

$colname_s_tgs = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_s_tgs = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_s_tgs = sprintf("SELECT * FROM pemohon, surat_tugas WHERE pemohon.id_pemohon=surat_tugas.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_s_tgs, "text"));
$s_tgs = mysql_query($query_s_tgs, $si_serkes_hewan) or die(mysql_error());
$row_s_tgs = mysql_fetch_assoc($s_tgs);
$totalRows_s_tgs = mysql_num_rows($s_tgs);

$colname_s_kes = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_s_kes = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_s_kes = sprintf("SELECT * FROM pemohon, sertifikat WHERE pemohon.id_pemohon=sertifikat.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_s_kes, "text"));
$s_kes = mysql_query($query_s_kes, $si_serkes_hewan) or die(mysql_error());
$row_s_kes = mysql_fetch_assoc($s_kes);
$totalRows_s_kes = mysql_num_rows($s_kes);

$colname_jns = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_jns = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_jns = sprintf("SELECT barang.jenis_hewan, barang.jumlah  FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_jns, "text"));
$jns = mysql_query($query_jns, $si_serkes_hewan) or die(mysql_error());
$row_jns = mysql_fetch_assoc($jns);
$totalRows_jns = mysql_num_rows($jns);

$colname_jmlh = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_jmlh = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_jmlh = sprintf("SELECT SUM(barang.jumlah) FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_jmlh, "text"));
$jmlh = mysql_query($query_jmlh, $si_serkes_hewan) or die(mysql_error());
$row_jmlh = mysql_fetch_assoc($jmlh);
$totalRows_jmlh = mysql_num_rows($jmlh);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<form id="form1" name="form1" method="get" action="surat_penugasan.php">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><div align="center">
        <table width="800" border="2" cellspacing="0" cellpadding="0" bgcolor="#D4D0C8">
          <tr>
            <td>ID Pemohon :
              <label for="id_pemohon5"></label>
              <label for="id_pemohon6"></label>
              <input type="text" name="id_pemohon" id="id_pemohon6" />
              <input type="submit" name="show" id="show" value="Preview" />
              <input name="close" type="button" id="close" onclick="window.print()" value="Print" /></td>
            </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td><div align="center">
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="left">.</div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="left">.</div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td width="20"><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong>REPUBLIK INDONESIA</strong></div></td>
            <td width="20"><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong>KEMENTRIAN PERTANIAN</strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong>BADAN KARANTINA PERTANIAN</strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td width="20"><div align="center">.</div></td>
            <td width="115"><div align="center"></div></td>
            <td colspan="2"><div align="center"></div></td>
            <td width="78"><div align="center"></div></td>
            <td width="78"><div align="center"></div></td>
            <td width="78"><div align="center"></div></td>
            <td width="161"><div align="center"></div></td>
            <td width="20"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong><em>REPUBLIC OF INDONESIA</em></strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong><em>MINISTRY OF AGRICULTURE</em></strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="center"><strong><em>AGENCY FOR AGRICULTURAL QUARANTINE</em></strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><hr align="left" />              </td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">Nomor</div></td>
            <td colspan="4"><div align="left">: <?php echo $row_s_tgs['no_srt']; ?></div></td>
            <td colspan="2"><div align="center">Tembilahan, <?php echo $row_s_tgs['tgl_surat_tgs']; ?></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">Lampiran</div></td>
            <td colspan="2"><div align="left">: </div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"><?php echo $row_dokter['nama']; ?></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">Perihal</div></td>
            <td colspan="2"><div align="left">: <u>Surat Penugasan</u></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left">Kepada Saudara <?php echo $row_s_tgs['nm_pemohon']; ?></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left">Di</div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left">Tembilahan</div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>.</td>
            <td>&nbsp;</td>
            <td colspan="5">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">.</div></td>
            <td width="209"><div align="left"></div></td>
            <td colspan="5"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="6"><div align="left">Berdasarkan Permohonan Pemeriksaan Karantina Perusahaan/ Pemilik <?php echo $row_s_tgs['nm_pemohon']; ?> No.<?php echo $row_s_kes['no_sertifikat']; ?> tanggal <?php echo $row_s_tgs['tgl_surat_tgs']; ?> untuk pemasukan/ pengeluaran dari/ ke <?php echo $row_s_tgs['daerah_asal']; ?> kepada saudara ditugaskan untuk melakukan pemeriksaan/ tindakan karantina terhadap Hewan</div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="7"><div align="left">.</div>              <div align="left"></div>              <div align="left"></div>              <div align="left"></div>              <div align="left"></div>              <div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="6"><div align="left">Demikianlah agar dilaksanakan dengan penuh tanggung jawab dan melaporkan hasil pemeriksaan selambat-lambatnya 1 (Satu) hari sejak surat penugasan ini dikeluarkan.</div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">.</div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left">.</div></td>
            <td colspan="6"><div align="left"></div>              <div align="left"></div>              <div align="left"></div>              <div align="left"></div>              <div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2">BKP. Kelas 1 Pekanbaru</td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2">Kepala UPT ............................</td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2">................................................</td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2">................................................</td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2">................................................</td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2"><u><?php echo $row_bkp['nama']; ?></u></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php echo $row_bkp['nip']; ?></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td colspan="2"><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><hr align="left" /></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="left">
              <table width="900" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="5"><div align="left">Hasil Pemeriksaan/ Diagnosa :</div></td>
                  </tr>
                <tr>
                  <td width="30"><div align="left"></div></td>
                  <td colspan="2"><div align="left">1. KONDISI HEWAN :</div></td>
                  <td width="155"><div align="left"></div></td>
                  <td width="30"><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td><div align="left">- JENIS</div></td>
                  <td><table border="0" cellpadding="0" cellspacing="0">
                    <?php do { ?>
                    <tr>
                      <td>: <?php echo $row_jns['jenis_hewan']; ?> : </td>
                      <td><?php echo $row_jns['jumlah']; ?></td>
                    </tr>
                    <?php } while ($row_jns = mysql_fetch_assoc($jns)); ?>
                  </table></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td width="153"><div align="left">- JUMLAH </div></td>
                  <td width="391"><div align="left">: <?php echo $row_jmlh['SUM(barang.jumlah)']; ?> EKOR</div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td><div align="left">- DAERAH ASAL </div></td>
                  <td><div align="left">: <?php echo $row_s_tgs['daerah_asal']; ?></div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td><div align="left">- DAERAH TUJUAN </div></td>
                  <td><div align="left">: <?php echo $row_s_tgs['daerah_tujuan']; ?></div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td><div align="left">- PEL. MUAT</div></td>
                  <td><div align="left">: <?php echo $row_s_tgs['pelabuhan_muat']; ?></div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td><div align="left">- PEL. BONGKAR</div></td>
                  <td><div align="left">: <?php echo $row_s_tgs['pelabuhan_bongkar']; ?></div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td colspan="2"><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td><div align="left"></div></td>
                  <td colspan="2"><div align="left">2. ALAT ANGKUT</div></td>
                  <td><div align="left"></div></td>
                  <td><div align="left"></div></td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">- <?php echo $row_s_tgs['alat_angkut']; ?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2"><?php echo $row_s_tgs['keterangan']; ?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left">Tembilahan, </div></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left">.........................................</div></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left">.........................................</div></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left">.........................................</div></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left"><?php echo $row_dokter['nama']; ?></div></td>
                  <td>&nbsp;</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                  <td><div align="left">Dokter Hewan Karantina</div></td>
                  <td>&nbsp;</td>
                  </tr>
              </table>
            </div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><div align="left">DISPOSISI KEPALA (PERINTAH LEBIH LANJUT)</div></td>
            <td><div align="left"></div></td>
            </tr>
        </table>
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($bkp);

mysql_free_result($dokter);

mysql_free_result($s_tgs);

mysql_free_result($s_kes);

mysql_free_result($jns);

mysql_free_result($jmlh);
?>
