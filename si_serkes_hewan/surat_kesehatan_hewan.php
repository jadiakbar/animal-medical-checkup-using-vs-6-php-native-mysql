<?php require_once('Connections/si_serkes_hewan.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_srt = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srt = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srt = sprintf("SELECT * FROM pemohon, sertifikat WHERE pemohon.id_pemohon=sertifikat.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_srt, "text"));
$srt = mysql_query($query_srt, $si_serkes_hewan) or die(mysql_error());
$row_srt = mysql_fetch_assoc($srt);
$totalRows_srt = mysql_num_rows($srt);

$colname_hwn = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_hwn = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_hwn = sprintf("SELECT * FROM pemohon, barang, barang_detail WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.jenis_hewan=barang_detail.jenis_hewan AND pemohon.id_pemohon=%s", GetSQLValueString($colname_hwn, "text"));
$hwn = mysql_query($query_hwn, $si_serkes_hewan) or die(mysql_error());
$row_hwn = mysql_fetch_assoc($hwn);
$totalRows_hwn = mysql_num_rows($hwn);

$colname_pemohon = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_pemohon = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_pemohon = sprintf("SELECT pemohon.nm_pemohon, pemohon.tgl_daftar FROM pemohon WHERE pemohon.id_pemohon=%s", GetSQLValueString($colname_pemohon, "text"));
$pemohon = mysql_query($query_pemohon, $si_serkes_hewan) or die(mysql_error());
$row_pemohon = mysql_fetch_assoc($pemohon);
$totalRows_pemohon = mysql_num_rows($pemohon);

$colname_srt_tgs = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srt_tgs = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srt_tgs = sprintf("SELECT * FROM pemohon, surat_tugas WHERE pemohon.id_pemohon=surat_tugas.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_srt_tgs, "text"));
$srt_tgs = mysql_query($query_srt_tgs, $si_serkes_hewan) or die(mysql_error());
$row_srt_tgs = mysql_fetch_assoc($srt_tgs);
$totalRows_srt_tgs = mysql_num_rows($srt_tgs);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_dokter = "SELECT * FROM nama_dokter";
$dokter = mysql_query($query_dokter, $si_serkes_hewan) or die(mysql_error());
$row_dokter = mysql_fetch_assoc($dokter);
$totalRows_dokter = mysql_num_rows($dokter);

$colname_tgl_klr = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_klr = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_klr = sprintf("SELECT DATE_FORMAT(sertifikat.tgl_dikeluarkan,  '%%d %%M %%Y') AS Tgl_kel FROM sertifikat WHERE sertifikat.id_pemohon=%s", GetSQLValueString($colname_tgl_klr, "text"));
$tgl_klr = mysql_query($query_tgl_klr, $si_serkes_hewan) or die(mysql_error());
$row_tgl_klr = mysql_fetch_assoc($tgl_klr);
$totalRows_tgl_klr = mysql_num_rows($tgl_klr);

$colname_tgl_hwn = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_hwn = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_hwn = sprintf("SELECT DATE_FORMAT(barang.tgl_barang, '%%d %%M %%Y') AS Tgl_hwn FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.id_pemohon=%s", GetSQLValueString($colname_tgl_hwn, "text"));
$tgl_hwn = mysql_query($query_tgl_hwn, $si_serkes_hewan) or die(mysql_error());
$row_tgl_hwn = mysql_fetch_assoc($tgl_hwn);
$totalRows_tgl_hwn = mysql_num_rows($tgl_hwn);

$colname_tgl_brgkat = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_brgkat = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_brgkat = sprintf("SELECT DATE_FORMAT(pemohon.tgl_daftar, '%%d %%M %%Y') AS Tgl_brgkat FROM pemohon WHERE pemohon.id_pemohon=%s", GetSQLValueString($colname_tgl_brgkat, "text"));
$tgl_brgkat = mysql_query($query_tgl_brgkat, $si_serkes_hewan) or die(mysql_error());
$row_tgl_brgkat = mysql_fetch_assoc($tgl_brgkat);
$totalRows_tgl_brgkat = mysql_num_rows($tgl_brgkat);

$i = 1; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.font1 {
	font-size: 34px;
}
.font2 {
	font-size: 30px;
}
.font3 {
	font-size: 22px;
}
.font_isi_bawah {
	font-size: 20px;
}
.font_judul {
	font-size: 24px;
}
</style>
</head>
<body>
<form id="form1" name="form1" method="get" action="surat_kesehatan_hewan.php">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><div align="center">
        <table width="1000" border="2" cellspacing="0" cellpadding="0" bgcolor="#D4D0C8">
          <tr>
            <td>ID Pemohon :
              <label for="id_pemohon5"></label>
              <label for="id_pemohon6"></label>
              <input type="text" name="id_pemohon" id="id_pemohon6" />
              <input type="submit" name="show" id="show" value="Preview" />
              <input name="close" type="button" id="close" onclick="window.print()" value="Print" /></td>
            </tr>
        </table>
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="179">.</td>
            <td width="821">&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td rowspan="5"><div align="right"><img src="gambar/600px-Kementerian_Pertanian_Republik_Indonesia.svg.png" width="200" height="193" /></div></td>
            <td><div align="center" class="font2"> <strong>PEMERINTAH KABUPATEN INDRAGIRI HILIR</strong>            </div></td>
          </tr>
          <tr>
            <td><div align="center" class="font1"> <strong>DINAS TANAMAN PANGAN,            </strong></div></td>
          </tr>
          <tr>
            <td><div align="center" class="font1"> <strong>HOLTIKULTURA DAN PETERNAKAN            </strong></div></td>
          </tr>
          <tr>
            <td><div align="center" class="font3">Jalan Pangeran Diponegoro No. 50 Tembilahan Kode Pos 29212</div></td>
          </tr>
          <tr>
            <td><div align="center" class="font3">Telepon (0768) 21094 Faks. (0768) 325516</div></td>
          </tr>
          <tr>
            <td colspan="2"><hr align="center" /></td>
            </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td><div align="center">
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8"><div align="left">.</div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_judul"><div align="center"><u>SURAT KETERANGAN KESEHATAN HEWAN</u></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td width="30"><div align="left"></div></td>
            <td colspan="7" class="font_judul"><div align="center">Nomor : <?php echo $row_srt['no_sertifikat']; ?></div></td>
            <td width="30" class="font_judul"><div align="center"></div></td>
            <td width="29"><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="8"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Yang bertanda tangan dibawah ini Dokter Hewan Berwenang Dinas Tanaman Pangan, Holtikultura dan Peternakan Kabupaten Indragiri Hilir, menerangkan bahwa pada tanggal  <?php echo $row_tgl_klr['Tgl_kel']; ?> telah diperiksa sebagaimana tersebut dibawah ini :</div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="10"><div align="center">
              <table width="900" border="1" cellpadding="2" cellspacing="0">
                <tr>
                  <td><div align="center"><strong><span class="font_isi_bawah">No</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Jenis Hewan</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Tipe Hewan</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Jumlah</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Uraian</span></strong></div></td>
                </tr>
                <?php do { ?>
                  <tr>
                    <td><div align="center"><span class="font_isi_bawah"><?php echo $i; ?></span></div></td>
                    <td class="font_isi_bawah"><?php echo $row_hwn['nama_brg']; ?></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['jenis_hewan']; ?></span></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['jumlah']; ?></span></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['uraian']; ?></span></td>
                  </tr> <?php $i++ ?>
                  <?php } while ($row_hwn = mysql_fetch_assoc($hwn)); ?>
              </table>
            </div>              
              <div align="center"></div>              <div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td width="30" class="font_isi_bawah"><div align="left"></div></td>
            <td width="179" class="font_isi_bawah">&nbsp;</td>
            <td colspan="4" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td width="119" class="font_isi_bawah">&nbsp;</td>
            <td width="119" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Setelah dilakukan pemriksaan hewan tersebut diatas dinyatakan sehat dan tidak menunjukkan tanda/ gejala penyakit hewan menular. Keterangan ini dibuat berdasarkan permintaan dari :  <?php echo $row_pemohon['nm_pemohon']; ?> pada tanggal <?php echo $row_tgl_brgkat['Tgl_brgkat']; ?></div>              <div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="left">Keterangan</div></td>
            <td colspan="4" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="3" class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="4" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">1. Nama Pemilik</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: <?php echo $row_srt_tgs['nm_pemohon']; ?></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">2. Alamat</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: <?php echo $row_srt_tgs['alamat']; ?></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">3. Daerah Asal</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: <?php echo $row_srt_tgs['daerah_asal']; ?></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">4. Daerah Tujuan</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: <?php echo $row_srt_tgs['daerah_tujuan']; ?></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left">5. Tanggal Berangkat </div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">: <?php echo $row_tgl_hwn['Tgl_hwn']; ?></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="5" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi_bawah"><div align="left">Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">DOKTER HEWAN BERWENANG</div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center"><u><?php echo $row_dokter['nama']; ?></u></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center">Nip. <?php echo $row_dokter['nip']; ?></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($srt);

mysql_free_result($hwn);

mysql_free_result($pemohon);

mysql_free_result($srt_tgs);

mysql_free_result($dokter);

mysql_free_result($tgl_klr);

mysql_free_result($tgl_hwn);

mysql_free_result($tgl_brgkat);
?>
