<?php require_once('Connections/si_serkes_hewan.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_bkp = "SELECT * FROM kepala_bkp";
$bkp = mysql_query($query_bkp, $si_serkes_hewan) or die(mysql_error());
$row_bkp = mysql_fetch_assoc($bkp);
$totalRows_bkp = mysql_num_rows($bkp);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_dokter = "SELECT * FROM nama_dokter";
$dokter = mysql_query($query_dokter, $si_serkes_hewan) or die(mysql_error());
$row_dokter = mysql_fetch_assoc($dokter);
$totalRows_dokter = mysql_num_rows($dokter);

$colname_s_kes = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_s_kes = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_s_kes = sprintf("SELECT * FROM pemohon, sertifikat WHERE pemohon.id_pemohon=sertifikat.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_s_kes, "text"));
$s_kes = mysql_query($query_s_kes, $si_serkes_hewan) or die(mysql_error());
$row_s_kes = mysql_fetch_assoc($s_kes);
$totalRows_s_kes = mysql_num_rows($s_kes);

$colname_jns = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_jns = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_jns = sprintf("SELECT barang.jenis_hewan, barang.jumlah  FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_jns, "text"));
$jns = mysql_query($query_jns, $si_serkes_hewan) or die(mysql_error());
$row_jns = mysql_fetch_assoc($jns);
$totalRows_jns = mysql_num_rows($jns);

$colname_jmlh = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_jmlh = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_jmlh = sprintf("SELECT SUM(barang.jumlah) FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_jmlh, "text"));
$jmlh = mysql_query($query_jmlh, $si_serkes_hewan) or die(mysql_error());
$row_jmlh = mysql_fetch_assoc($jmlh);
$totalRows_jmlh = mysql_num_rows($jmlh);

$colname_srt_tgs = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srt_tgs = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srt_tgs = sprintf("SELECT DATE_FORMAT(surat_tugas.tgl_surat_tgs, '%%d %%M %%Y') FROM surat_tugas WHERE surat_tugas.id_pemohon=%s", GetSQLValueString($colname_srt_tgs, "text"));
$srt_tgs = mysql_query($query_srt_tgs, $si_serkes_hewan) or die(mysql_error());
$row_srt_tgs = mysql_fetch_assoc($srt_tgs);
$totalRows_srt_tgs = mysql_num_rows($srt_tgs);

$colname_tgl_surat = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_surat = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_surat = sprintf("SELECT DATE_FORMAT(surat_tugas.tgl_surat_tgs, '%%d %%M %%Y') AS Tanggal FROM surat_tugas WHERE surat_tugas.id_pemohon=%s", GetSQLValueString($colname_tgl_surat, "text"));
$tgl_surat = mysql_query($query_tgl_surat, $si_serkes_hewan) or die(mysql_error());
$row_tgl_surat = mysql_fetch_assoc($tgl_surat);
$totalRows_tgl_surat = mysql_num_rows($tgl_surat);

$colname_s_tgs = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_s_tgs = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_s_tgs = sprintf("SELECT * FROM surat_tugas WHERE surat_tugas.id_pemohon=%s", GetSQLValueString($colname_s_tgs, "text"));
$s_tgs = mysql_query($query_s_tgs, $si_serkes_hewan) or die(mysql_error());
$row_s_tgs = mysql_fetch_assoc($s_tgs);
$totalRows_s_tgs = mysql_num_rows($s_tgs);

$colname_tgl_muat = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_muat = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_muat = sprintf("SELECT DATE_FORMAT( barang.tgl_barang, '%%d %%M %%Y') AS tgl_mt FROM barang WHERE barang.id_pemohon=%s", GetSQLValueString($colname_tgl_muat, "text"));
$tgl_muat = mysql_query($query_tgl_muat, $si_serkes_hewan) or die(mysql_error());
$row_tgl_muat = mysql_fetch_assoc($tgl_muat);
$totalRows_tgl_muat = mysql_num_rows($tgl_muat);

$colname_srtf = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srtf = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srtf = sprintf("SELECT barang.jenis_hewan, barang.jumlah, sertifikat.no_sertifikat, sertifikat.no_pengesahan FROM barang, sertifikat WHERE sertifikat.id_pemohon=barang.id_pemohon AND sertifikat.id_pemohon=%s", GetSQLValueString($colname_srtf, "text"));
$srtf = mysql_query($query_srtf, $si_serkes_hewan) or die(mysql_error());
$row_srtf = mysql_fetch_assoc($srtf);
$totalRows_srtf = mysql_num_rows($srtf);

$colname_hwn = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_hwn = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_hwn = sprintf("SELECT barang.jenis_hewan FROM barang WHERE barang.id_pemohon=%s", GetSQLValueString($colname_hwn, "text"));
$hwn = mysql_query($query_hwn, $si_serkes_hewan) or die(mysql_error());
$row_hwn = mysql_fetch_assoc($hwn);
$totalRows_hwn = mysql_num_rows($hwn);

$colname_j = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_j = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_j = sprintf("SELECT barang.jumlah FROM barang WHERE barang.id_pemohon=%s", GetSQLValueString($colname_j, "text"));
$j = mysql_query($query_j, $si_serkes_hewan) or die(mysql_error());
$row_j = mysql_fetch_assoc($j);
$totalRows_j = mysql_num_rows($j);

$colname_s_ket = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_s_ket = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_s_ket = sprintf("SELECT sertifikat.no_sertifikat FROM sertifikat WHERE sertifikat.id_pemohon=%s", GetSQLValueString($colname_s_ket, "text"));
$s_ket = mysql_query($query_s_ket, $si_serkes_hewan) or die(mysql_error());
$row_s_ket = mysql_fetch_assoc($s_ket);
$totalRows_s_ket = mysql_num_rows($s_ket);

$colname_tot = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tot = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tot = sprintf("SELECT SUM(barang.jumlah) FROM barang WHERE barang.id_pemohon=%s", GetSQLValueString($colname_tot, "text"));
$tot = mysql_query($query_tot, $si_serkes_hewan) or die(mysql_error());
$row_tot = mysql_fetch_assoc($tot);
$totalRows_tot = mysql_num_rows($tot);
?>
<?php $i=1; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.font_isi {
	font-size: 26px;
}
.font_isi_bawah {
	font-size: 18px;
	font-family: Verdana, Geneva, sans-serif;
}
.judul {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-size: 24px;
}
</style>
</head>

<body>
<form id="form1" name="form1" method="get" action="sertifikat_kesehatan.php">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><div align="center">
        <table width="1000" border="2" cellspacing="0" cellpadding="0" bgcolor="#D4D0C8">
          <tr>
            <td>ID Pemohon :
              <label for="id_pemohon5"></label>
              <label for="id_pemohon6"></label>
              <input type="text" name="id_pemohon" id="id_pemohon6" />
              <input type="submit" name="show" id="show" value="Preview" />
              <input name="close" type="button" id="close" onclick="window.print()" value="Print" /></td>
            </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td><div align="center">
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="9"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="9">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="9"><div align="left">.</div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td width="4"><div align="left"></div></td>
            <td colspan="7"><div align="center" class="font_isi"><strong>REPUBLIK INDONESIA</strong></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="9"><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi"><div align="center"><strong>KEMENTRIAN PERTANIAN</strong></div></td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi"><div align="center"><strong>BADAN KARANTINA PERTANIAN</strong></div></td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td width="7" class="font_isi"><div align="center"></div></td>
            <td width="105" class="font_isi"><div align="center"></div></td>
            <td colspan="2" class="font_isi"><div align="center"></div></td>
            <td width="83" class="font_isi"><div align="center"></div></td>
            <td width="83" class="font_isi"><div align="center"></div></td>
            <td width="186" class="font_isi"><div align="center"></div></td>
            <td width="220" class="font_isi"><div align="center"></div></td>
            <td width="43" class="font_isi"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi"><div align="center"><strong><em>REPUBLIC OF INDONESIA</em></strong></div></td>
            <td colspan="2" rowspan="3" class="font_isi"><div align="left">
              <table width="300" border="1" cellspacing="0" cellpadding="0">
                <tr>
                  <td>No. <?php echo $row_srtf['no_pengesahan']; ?></td>
                  </tr>
                <tr>
                  <td><?php echo $row_s_ket['no_sertifikat']; ?></td>
                  </tr>
              </table>
            </div>
              <div align="left"></div>              <div align="left"></div></td>
            <td rowspan="3" class="font_isi">&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi"><div align="center"><strong><em>MINISTRY OF AGRICULTURE</em></strong></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="7" class="font_isi"><div align="center"><strong><em>AGENCY FOR AGRICULTURAL QUARANTINE</em></strong></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">KH - 9</td>
            <td width="156" class="font_isi">&nbsp;</td>
            <td width="204" class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td class="font_isi">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9"><hr align="left" />              </td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="4" class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9" class="judul"><div align="center"><u>SERTIFIKAT KESEHATAN HEWAN</u></div></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9" class="judul"><div align="center">ANIMAL HEALT CERTIFICATE</div></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="9" class="font_isi_bawah"><div align="left">
              <table width="920" border="1" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="500"><table width="454" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="259"><div align="left"><u>Negara/Daerah Asal*)</u></div></td>
                      <td width="11" rowspan="2"><div align="left">: </div></td>
                      <td width="200" rowspan="2"><div align="left"><?php echo $row_s_tgs['daerah_asal']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Country/Place of Origin*)</em></div></td>
                      </tr>
                    </table></td>
                  <td width="534"><table width="540" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="282"><div align="left"><u>Negara/Daerah Tujuan*)</u></div></td>
                      <td width="11" rowspan="2"><div align="left">: </div></td>
                      <td width="277" rowspan="2"><div align="left"><?php echo $row_s_tgs['daerah_tujuan']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Country/Place of Destination*)</em></div></td>
                      </tr>
                    </table></td>
                  </tr>
                <tr>
                  <td><table width="454" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="259"><div align="left"><u>Pelabuhan dan tanggal muat</u></div></td>
                      <td width="11" rowspan="2"><div align="left">: </div></td>
                      <td width="200" rowspan="2"><div align="left"><?php echo $row_s_tgs['pelabuhan_muat']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Date and Place of Embarkation</em></div></td>
                      </tr>
                    </table></td>
                  <td><table width="540" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="282"><div align="left"><u>Nama dan Alamat Penerima</u></div></td>
                      <td width="11"><div align="left">: </div></td>
                      <td width="277"><div align="left"><?php echo $row_s_kes['nm_pemohon']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Name and Address of Consignee</em></div></td>
                      <td><div align="left">: </div></td>
                      <td><div align="left"><?php echo $row_s_kes['alamat']; ?></div></td>
                      </tr>
                    </table></td>
                  </tr>
                <tr>
                  <td><table width="454" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="259"><div align="left"><u>Pelabuhan dan tanggal muat </u></div></td>
                      <td width="11"><div align="left">: </div></td>
                      <td width="200"><div align="left"><?php echo $row_s_tgs['pelabuhan_muat']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Date and Place of Embarkation</em></div></td>
                      <td><div align="left">: </div></td>
                      <td><div align="left"><?php echo $row_tgl_muat['tgl_mt']; ?></div></td>
                      </tr>
                    </table></td>
                  <td><table width="540" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="282"><div align="left"><u>Pelabuhan bongkar</u></div></td>
                      <td width="11" rowspan="2"><div align="left">: </div></td>
                      <td width="277" rowspan="2"><div align="left"><?php echo $row_s_tgs['pelabuhan_bongkar']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Port of Disembarkation</em></div></td>
                      </tr>
                    </table></td>
                  </tr>
                <tr>
                  <td colspan="2"><table width="920" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="391"><div align="left"><u>Nama Alat angkut/No</u></div></td>
                      <td width="14" rowspan="2"><div align="left">: </div></td>
                      <td width="635" rowspan="2"><div align="left"><?php echo $row_s_tgs['alat_angkut']; ?></div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Means of transportation/Number</em></div></td>
                      </tr>
                    </table></td>
                  </tr>
              </table>
            </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="9" class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="9" class="font_isi_bawah"><div align="left">
              <table width="1000" border="1" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="42"><div align="center">
                    <strong><br>
                      <u>No</u></br>
                      <em>No</em> </strong>
                    <p></p>
                    </div></td>
                  <td width="357"><div align="center">
                    <strong><br>
                      <u>Jenis Hewan</u></br>
                      <em>Kind of animal (s)
                        </em></strong>
                    <p></p>
                    </div></td>
                  <td width="210"><div align="center">
                    <strong><br>
                      <u>Jumlah Hewan</u></br>
                      <em>Number of animals</em> </strong>
                    <p></p>
                    </div></td>
                  <td width="425"><div align="center">
                    <strong><br>
                      <u>Keterangan Hewan</u></br>
                      <em>Description of animals</em> </strong>
                    <p></p>
                    </div></td>
                  </tr>
                <tr>
                  <td><div align="center"></div></td>
                  <td><div align="center">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <?php do { ?>
                        <tr>
                          <td><?php echo $row_hwn['jenis_hewan']; ?></td>
                          </tr>
                        <?php } while ($row_hwn = mysql_fetch_assoc($hwn)); ?>
                      </table>
                    </div></td>
                  <td><div align="center">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <?php do { ?>
                        <tr>
                          <td><?php echo $row_j['jumlah']; ?> Ekor</td>
                          </tr>
                        <?php } while ($row_j = mysql_fetch_assoc($j)); ?>
                      </table>
                    </div></td>
                  <td><div align="center"><?php echo $row_s_ket['no_sertifikat']; ?></div></td>
                  </tr>
                <tr>
                  <td colspan="2"><div align="center"><strong>Total </strong></div></td>
                  <td><div align="center"><strong><?php echo $row_tot['SUM(barang.jumlah)']; ?> Ekor</strong></div></td>
                  <td>&nbsp;</td>
                  </tr>
              </table>
            </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="9" class="font_isi_bawah"><table width="1040" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="3" class="font_isi_bawah">&nbsp;</td>
            <td rowspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="3" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="7" class="font_isi_bawah"><div align="left">Dengan ini saya menerangkan bahwa :</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="7" class="font_isi_bawah"><div align="left"><em>I hereby certify</em></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left">
              <input type="checkbox" name="cek1" id="cek1" />
            </div>
              <label for="cek1"></label></td>
            <td colspan="6" class="font_isi_bawah"><div align="left"><u>Hewan tersebut diatas telah dilakukan pemeriksaan dan bebas dari penyakit infeksi dan penyakit menular serta bebas dari ekto parasit</u></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="6" class="font_isi_bawah"><div align="left"><em>The animal described above has been inspected and found free from infectios and contatious deseases and free from ecto parasites</em></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left">
              <input type="checkbox" name="cek2" id="cek2" />
            </div>
              <label for="cek2"></label></td>
            <td colspan="6" class="font_isi_bawah"><div align="left"><u>Hewan dalam keadaan sehat dan layak untuk diberangkatkan</u></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="6" class="font_isi_bawah"><div align="left"><em>The animal are healty and in good condition for transportation</em></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="9" class="font_isi_bawah"><div align="left">
              <table width="1000" border="1" cellspacing="0" cellpadding="0">
                <tr>
                  <td><div align="center">
                    <table width="1000" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="353"><div align="center">
                          <p>&nbsp;</p>
                          <p>&nbsp;</p>
                          <p>&nbsp;</p>
                          <p>&nbsp;</p><u>Tanda Tangan</u>
                          </div></td>
                        <td width="353"><div align="center">
                          <p>&nbsp;</p>
                          <p><?php echo $row_dokter['nama']; ?></p>
                          <p>&nbsp;</p>
                          <p>&nbsp;</p><u>Dokter Hewan Karantina</u>
                          </div></td>
                        <td width="354"><div align="center">
                          <p>&nbsp;</p>
                          <p>&nbsp;</p>
                          <p>&nbsp;</p>
                          <p>&nbsp;</p><u>Stempel</u></div></td>
                        </tr>
                      <tr>
                        <td><div align="center"><em>Signature</em></div></td>
                        <td><div align="center"><em>Official Quarantine Veterinarian</em></div></td>
                        <td><div align="center"><em>Official Stamp</em></div></td>
                        </tr>
                      </table>
                    </div></td>
                  </tr>
                <tr>
                  <td><table width="960" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="353"><div align="left"><u>Tanggal dikeluarkan</u></div></td>
                      <td width="353" rowspan="2"><div align="left">: <?php echo $row_tgl_muat['tgl_mt']; ?></div></td>
                      <td width="354"><div align="left"><u>di :</u> Tembilahan</div></td>
                      </tr>
                    <tr>
                      <td><div align="left"><em>Date of issued</em></div></td>
                      <td><div align="left"><em>at</em></div></td>
                      </tr>
                    </table></td>
                  </tr>
              </table>
            </div></td>
            <td>&nbsp;</td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
        </table>
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($bkp);

mysql_free_result($dokter);

mysql_free_result($s_kes);

mysql_free_result($jns);

mysql_free_result($jmlh);

mysql_free_result($srt_tgs);

mysql_free_result($tgl_surat);

mysql_free_result($s_tgs);

mysql_free_result($tgl_muat);

mysql_free_result($srtf);

mysql_free_result($hwn);

mysql_free_result($j);

mysql_free_result($s_ket);

mysql_free_result($tot);
?>
