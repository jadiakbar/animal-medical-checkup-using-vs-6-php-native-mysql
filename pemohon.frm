VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form Entri_Pemohon 
   Caption         =   "Data Pemohon"
   ClientHeight    =   7650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10530
   LinkTopic       =   "Form2"
   ScaleHeight     =   7650
   ScaleWidth      =   10530
   StartUpPosition =   2  'CenterScreen
   Begin MSAdodcLib.Adodc Ado_pemohon 
      Height          =   375
      Left            =   0
      Top             =   7080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "pemohon"
      Caption         =   "Data Pemohon"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "pemohon.frx":0000
      Height          =   3615
      Left            =   0
      TabIndex        =   19
      Top             =   3960
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   6376
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame3 
      Caption         =   "Filter"
      Height          =   975
      Left            =   0
      TabIndex        =   14
      Top             =   2880
      Width           =   9135
      Begin VB.CommandButton Cmdkeluar 
         Caption         =   "&Keluar"
         DragIcon        =   "pemohon.frx":001A
         Height          =   615
         Left            =   7920
         Picture         =   "pemohon.frx":0B1C
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Cmdberikut 
         DownPicture     =   "pemohon.frx":161E
         Height          =   495
         Left            =   1680
         Picture         =   "pemohon.frx":2020
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdterakhir 
         Height          =   495
         Left            =   2400
         Picture         =   "pemohon.frx":2A22
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdpertama 
         Height          =   495
         Left            =   240
         Picture         =   "pemohon.frx":3424
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdsebelum 
         Height          =   495
         Left            =   960
         Picture         =   "pemohon.frx":3E26
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   9120
      TabIndex        =   1
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Cmdbatal 
         Caption         =   "&Batal"
         Height          =   615
         Left            =   120
         Picture         =   "pemohon.frx":4828
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton Cmdtambah 
         Caption         =   "&Tambah"
         Height          =   615
         Left            =   120
         Picture         =   "pemohon.frx":522A
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Cmdsimpan 
         Caption         =   "&Simpan"
         Height          =   615
         Left            =   120
         Picture         =   "pemohon.frx":5C2C
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton Cmdedit 
         Caption         =   "&Edit"
         Height          =   615
         Left            =   120
         Picture         =   "pemohon.frx":662E
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton Cmdhapus 
         Caption         =   "&Hapus"
         Height          =   615
         Left            =   120
         Picture         =   "pemohon.frx":7030
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   3120
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Entri Pemohon"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      Begin VB.TextBox txt_no_telp 
         Height          =   375
         Left            =   2400
         TabIndex        =   12
         Top             =   2160
         Width           =   2175
      End
      Begin VB.TextBox txt_alamat 
         Height          =   615
         Left            =   2400
         TabIndex        =   11
         Top             =   1440
         Width           =   2775
      End
      Begin VB.TextBox txt_nm_pemohon 
         Height          =   375
         Left            =   2400
         TabIndex        =   10
         Top             =   960
         Width           =   2175
      End
      Begin VB.TextBox txt_id_pemohon 
         Height          =   375
         Left            =   2400
         TabIndex        =   9
         Top             =   480
         Width           =   2175
      End
      Begin VB.Label Label4 
         Caption         =   "Nomor Telp/ HP"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   2280
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Alamat"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1560
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Nama Pemohon"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label1 
         Caption         =   "Identitas Pemohon"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   2295
      End
   End
End
Attribute VB_Name = "Entri_Pemohon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Baru As Boolean

Private Sub Cmdbatal_Click()
Tombol True, True, False, False, True
Ado_pemohon.Recordset.Cancel
Kosong
End Sub

Private Sub Cmdberikut_Click()
'Menuju ke record berikutnya
Ado_pemohon.Recordset.MoveNext
'Jika berada di record terakhir menuju ke record terakhir
If Ado_pemohon.Recordset.EOF Then
Ado_pemohon.Recordset.MoveLast
End If
End Sub


Private Sub Cmdedit_Click()
Tombol False, False, True, True, False
With Ado_pemohon.Recordset
txt_id_pemohon = !id_pemohon
txt_nm_pemohon = !nm_pemohon
txt_alamat = !alamat
txt_no_telp = !no_telp
End With
txt_id_pemohon.SetFocus
Baru = False
End Sub

Private Sub Cmdhapus_Click()
Dim hapus
hapus = MsgBox("Apakah anda yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus Data")
If hapus = vbYes Then
    Ado_pemohon.Recordset.Delete
    Ado_pemohon.Recordset.MoveLast
Else
    MsgBox "Data tidak jadi dihapus!", vbOKOnly + vbInformation, "Batal menghapus"
End If
End Sub

Private Sub Cmdkeluar_Click()
Unload Me
End Sub

Private Sub Cmdpertama_Click()
'Menuju ke record pertama
Ado_pemohon.Recordset.MoveFirst
End Sub

Private Sub Cmdsebelum_Click()
'Menuju ke record sebelumnya
Ado_pemohon.Recordset.MovePrevious
'Jika berada di record pertama menuju ke record pertama
If Ado_pemohon.Recordset.BOF Then
Ado_pemohon.Recordset.MoveFirst
End If
End Sub

Private Sub Cmdsimpan_Click()
Tombol True, True, False, False, True
With Ado_pemohon.Recordset
If Baru Then .AddNew
!id_pemohon = txt_id_pemohon.Text
!nm_pemohon = txt_nm_pemohon.Text
!alamat = txt_alamat.Text
!no_telp = txt_no_telp.Text
.Update
.Sort = "id_pemohon"
End With
Kosong
End Sub

Private Sub Cmdtambah_Click()
Tombol False, False, True, True, False
Baru = True
Kosong
txt_id_pemohon.SetFocus
End Sub

Private Sub Cmdterakhir_Click()
Ado_pemohon.Recordset.MoveLast
End Sub

Private Sub Form_Load()
Ado_pemohon.ConnectionString = "DSN=si_serkes_hewan"
Ado_pemohon.RecordSource = "pemohon"
Ado_pemohon.Refresh
Ado_pemohon.Recordset.Sort = "id_pemohon"
Baru = False
End Sub

Public Sub Tombol(tambah, edit, simpan, batal, hapus As Boolean)
Cmdtambah.Enabled = tambah
Cmdedit.Enabled = edit
Cmdsimpan.Enabled = simpan
Cmdbatal.Enabled = batal
Cmdhapus.Enabled = hapus
End Sub

Public Sub Kosong()
txt_id_pemohon.Text = ""
txt_nm_pemohon.Text = ""
txt_alamat.Text = ""
txt_no_telp.Text = ""
End Sub
