VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Entri_Sertifikat 
   Caption         =   "Data Sertifikat"
   ClientHeight    =   7650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10530
   LinkTopic       =   "Form1"
   ScaleHeight     =   7650
   ScaleWidth      =   10530
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame Frame3 
      Caption         =   "Filter"
      Height          =   975
      Left            =   0
      TabIndex        =   14
      Top             =   2880
      Width           =   9135
      Begin VB.CommandButton Cmdkeluar 
         Caption         =   "&Keluar"
         DragIcon        =   "sertifikat.frx":0000
         Height          =   615
         Left            =   7920
         Picture         =   "sertifikat.frx":0B02
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Cmdberikut 
         DownPicture     =   "sertifikat.frx":1604
         Height          =   495
         Left            =   1680
         Picture         =   "sertifikat.frx":2006
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdterakhir 
         Height          =   495
         Left            =   2400
         Picture         =   "sertifikat.frx":2A08
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdpertama 
         Height          =   495
         Left            =   240
         Picture         =   "sertifikat.frx":340A
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdsebelum 
         Height          =   495
         Left            =   960
         Picture         =   "sertifikat.frx":3E0C
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   9120
      TabIndex        =   8
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Cmdbatal 
         Caption         =   "&Batal"
         Height          =   615
         Left            =   120
         Picture         =   "sertifikat.frx":480E
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton Cmdtambah 
         Caption         =   "&Tambah"
         Height          =   615
         Left            =   120
         Picture         =   "sertifikat.frx":5210
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Cmdsimpan 
         Caption         =   "&Simpan"
         Height          =   615
         Left            =   120
         Picture         =   "sertifikat.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton Cmdedit 
         Caption         =   "&Edit"
         Height          =   615
         Left            =   120
         Picture         =   "sertifikat.frx":6614
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton Cmdhapus 
         Caption         =   "&Hapus"
         Height          =   615
         Left            =   120
         Picture         =   "sertifikat.frx":7016
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   3120
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Entri Sertifikat"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      Begin VB.TextBox txt_id_sert 
         Enabled         =   0   'False
         Height          =   375
         Left            =   6240
         Locked          =   -1  'True
         TabIndex        =   24
         Top             =   2040
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txt_id_pemohon 
         Height          =   375
         Left            =   2400
         TabIndex        =   22
         Top             =   1560
         Width           =   2175
      End
      Begin VB.TextBox txt_no_pengesahan 
         Height          =   375
         Left            =   2400
         TabIndex        =   21
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox txt_keterangan 
         Height          =   855
         Left            =   6240
         TabIndex        =   3
         Top             =   1080
         Width           =   2655
      End
      Begin VB.TextBox txt_no_sertifikat 
         Height          =   375
         Left            =   2400
         TabIndex        =   2
         Top             =   600
         Width           =   3495
      End
      Begin MSComCtl2.DTPicker txt_tgl_dikeluarkan 
         Bindings        =   "sertifikat.frx":7A18
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "yyyy-MM-dd"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   375
         Left            =   2400
         TabIndex        =   1
         Top             =   2040
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "yyyy-mm-dd"
         Format          =   16121859
         CurrentDate     =   41886
      End
      Begin VB.Label Label5 
         Caption         =   "Tanggal Dikeluarkan"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   2160
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Keterangan"
         Height          =   255
         Left            =   5040
         TabIndex        =   7
         Top             =   1200
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Identitas Pemohon"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "Nomor Sertifikat"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Nomor Pengesahan"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1200
         Width           =   1575
      End
   End
   Begin MSAdodcLib.Adodc Ado_sertifikat 
      Height          =   375
      Left            =   0
      Top             =   7080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "sertifikat"
      Caption         =   "Data Sertifikat"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "sertifikat.frx":7A40
      Height          =   3615
      Left            =   0
      TabIndex        =   20
      Top             =   3960
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   6376
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Entri_Sertifikat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Baru As Boolean

Private Sub Cmdbatal_Click()
Tombol True, True, False, False, True
Ado_sertifikat.Recordset.Cancel
Kosong
End Sub

Private Sub Cmdberikut_Click()
'Menuju ke record berikutnya
Ado_sertifikat.Recordset.MoveNext
'Jika berada di record terakhir menuju ke record terakhir
If Ado_sertifikat.Recordset.EOF Then
Ado_sertifikat.Recordset.MoveLast
End If
End Sub

Private Sub Cmdedit_Click()
Tombol False, False, True, True, False
With Ado_sertifikat.Recordset
txt_no_sertifikat = !no_sertifikat
txt_no_pengesahan = !no_pengesahan
txt_id_pemohon = !id_pemohon
txt_keterangan = !keterangan
txt_tgl_dikeluarkan = !tgl_dikeluarkan
End With
Baru = False
End Sub

Private Sub Cmdhapus_Click()
Dim hapus
hapus = MsgBox("Apakah anda yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus Data")
If hapus = vbYes Then
    Ado_sertifikat.Recordset.Delete
    Ado_sertifikat.Recordset.MoveLast
Else
    MsgBox "Data tidak jadi dihapus!", vbOKOnly + vbInformation, "Batal menghapus"
End If
End Sub

Private Sub Cmdkeluar_Click()
Unload Me
End Sub

Private Sub Cmdpertama_Click()
'Menuju ke record pertama
Ado_sertifikat.Recordset.MoveFirst
End Sub

Private Sub Cmdsebelum_Click()
'Menuju ke record sebelumnya
Ado_sertifikat.Recordset.MovePrevious
'Jika berada di record pertama menuju ke record pertama
If Ado_sertifikat.Recordset.BOF Then
Ado_sertifikat.Recordset.MoveFirst
End If
End Sub

Private Sub Cmdsimpan_Click()
Tombol True, True, False, False, True
With Ado_sertifikat.Recordset
If Baru Then .AddNew
!no_sertifikat = txt_no_sertifikat.Text
!no_pengesahan = txt_no_pengesahan.Text
!id_pemohon = txt_id_pemohon.Text
!keterangan = txt_keterangan.Text
!tgl_dikeluarkan = txt_tgl_dikeluarkan.Value
.Update
.Sort = "id_sert"
End With
Kosong
End Sub

Private Sub Cmdtambah_Click()
Tombol False, False, True, True, False
Baru = True
Kosong
End Sub

Private Sub Cmdterakhir_Click()
Ado_sertifikat.Recordset.MoveLast
End Sub

Private Sub Form_Load()
Ado_sertifikat.ConnectionString = "DSN=si_serkes_hewan"
Ado_sertifikat.RecordSource = "sertifikat"
Ado_sertifikat.Refresh
Ado_sertifikat.Recordset.Sort = "id_sert"
Baru = False
End Sub

Public Sub Tombol(tambah, edit, simpan, batal, hapus As Boolean)
Cmdtambah.Enabled = tambah
Cmdedit.Enabled = edit
Cmdsimpan.Enabled = simpan
Cmdbatal.Enabled = batal
Cmdhapus.Enabled = hapus
End Sub

Public Sub Kosong()
txt_no_sertifikat.Text = ""
txt_no_pengesahan.Text = ""
txt_id_pemohon.Text = ""
txt_keterangan.Text = ""
End Sub
