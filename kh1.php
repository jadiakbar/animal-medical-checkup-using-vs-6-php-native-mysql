<?php require_once('Connections/si_serkes_hewan.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_srt = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srt = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srt = sprintf("SELECT * FROM pemohon, sertifikat WHERE pemohon.id_pemohon=sertifikat.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_srt, "text"));
$srt = mysql_query($query_srt, $si_serkes_hewan) or die(mysql_error());
$row_srt = mysql_fetch_assoc($srt);
$totalRows_srt = mysql_num_rows($srt);

$colname_hwn = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_hwn = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_hwn = sprintf("SELECT * FROM pemohon, barang, barang_detail WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.jenis_hewan=barang_detail.jenis_hewan AND pemohon.id_pemohon=%s", GetSQLValueString($colname_hwn, "text"));
$hwn = mysql_query($query_hwn, $si_serkes_hewan) or die(mysql_error());
$row_hwn = mysql_fetch_assoc($hwn);
$totalRows_hwn = mysql_num_rows($hwn);

$colname_pemohon = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_pemohon = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_pemohon = sprintf("SELECT pemohon.nm_pemohon, pemohon.tgl_daftar FROM pemohon WHERE pemohon.id_pemohon=%s", GetSQLValueString($colname_pemohon, "text"));
$pemohon = mysql_query($query_pemohon, $si_serkes_hewan) or die(mysql_error());
$row_pemohon = mysql_fetch_assoc($pemohon);
$totalRows_pemohon = mysql_num_rows($pemohon);

$colname_srt_tgs = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_srt_tgs = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_srt_tgs = sprintf("SELECT * FROM pemohon, surat_tugas WHERE pemohon.id_pemohon=surat_tugas.id_pemohon AND pemohon.id_pemohon=%s", GetSQLValueString($colname_srt_tgs, "text"));
$srt_tgs = mysql_query($query_srt_tgs, $si_serkes_hewan) or die(mysql_error());
$row_srt_tgs = mysql_fetch_assoc($srt_tgs);
$totalRows_srt_tgs = mysql_num_rows($srt_tgs);

mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_dokter = "SELECT * FROM nama_dokter";
$dokter = mysql_query($query_dokter, $si_serkes_hewan) or die(mysql_error());
$row_dokter = mysql_fetch_assoc($dokter);
$totalRows_dokter = mysql_num_rows($dokter);

$colname_tgl_klr = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_klr = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_klr = sprintf("SELECT DATE_FORMAT(sertifikat.tgl_dikeluarkan,  '%%d %%M %%Y') AS Tgl_kel FROM sertifikat WHERE sertifikat.id_pemohon=%s", GetSQLValueString($colname_tgl_klr, "text"));
$tgl_klr = mysql_query($query_tgl_klr, $si_serkes_hewan) or die(mysql_error());
$row_tgl_klr = mysql_fetch_assoc($tgl_klr);
$totalRows_tgl_klr = mysql_num_rows($tgl_klr);

$colname_tgl_hwn = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_hwn = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_hwn = sprintf("SELECT DATE_FORMAT(barang.tgl_barang, '%%d %%M %%Y') AS Tgl_hwn FROM pemohon, barang WHERE pemohon.id_pemohon=barang.id_pemohon AND barang.id_pemohon=%s", GetSQLValueString($colname_tgl_hwn, "text"));
$tgl_hwn = mysql_query($query_tgl_hwn, $si_serkes_hewan) or die(mysql_error());
$row_tgl_hwn = mysql_fetch_assoc($tgl_hwn);
$totalRows_tgl_hwn = mysql_num_rows($tgl_hwn);

$colname_tgl_brgkat = "-1";
if (isset($_GET['id_pemohon'])) {
  $colname_tgl_brgkat = $_GET['id_pemohon'];
}
mysql_select_db($database_si_serkes_hewan, $si_serkes_hewan);
$query_tgl_brgkat = sprintf("SELECT DATE_FORMAT(pemohon.tgl_daftar, '%%d %%M %%Y') AS Tgl_brgkat FROM pemohon WHERE pemohon.id_pemohon=%s", GetSQLValueString($colname_tgl_brgkat, "text"));
$tgl_brgkat = mysql_query($query_tgl_brgkat, $si_serkes_hewan) or die(mysql_error());
$row_tgl_brgkat = mysql_fetch_assoc($tgl_brgkat);
$totalRows_tgl_brgkat = mysql_num_rows($tgl_brgkat);

$i = 1; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.font1 {
	font-size: 34px;
}
.font2 {
	font-size: 30px;
	font-family: Verdana, Geneva, sans-serif;
}
.font3 {
	font-size: 22px;
	font-family: Verdana, Geneva, sans-serif;
}
.font_isi_bawah {
	font-size: 20px;
	font-family: Verdana, Geneva, sans-serif;
}
.font_judul {
	font-size: 24px;
	font-family: Verdana, Geneva, sans-serif;
}
.diisi_petugas {
	font-size: 10px;
}
</style>
</head>
<body>
<form id="form1" name="form1" method="get" action="kh1.php">
  <table width="1000" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><div align="center">
        <table width="1000" border="2" cellspacing="0" cellpadding="0" bgcolor="#D4D0C8">
          <tr>
            <td>ID Pemohon :
              <label for="id_pemohon5"></label>
              <label for="id_pemohon6"></label>
              <input type="text" name="id_pemohon" id="id_pemohon6" />
              <input type="submit" name="show" id="show" value="Preview" />
              <input name="close" type="button" id="close" onclick="window.print()" value="Print" /></td>
            </tr>
        </table>
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="150">.</td>
            <td width="700">&nbsp;</td>
            <td width="150">&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.              </td>
            <td class="font_judul"><div align="center">REPUBLIK INDONESIA</div></td>
            <td class="font_judul">&nbsp;</td>
          </tr>
          <tr>
            <td rowspan="6"><div align="center"><img src="gambar/logo kementeriian pertanian.jpg" width="150" height="150" /></div></td>
            <td><div align="center"><span class="font_judul">KEMENTRIAN PERTANIAN</span></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center"><span class="font_judul">BADAN KARANTINA PERTANIAN</span></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center"><em><strong class="font_judul">REPUBLIC OF INDONESIA</strong></em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center" class="font1"> <em><span class="font_judul"><strong>MINISTRY OF AGRICULTURE</strong></span></em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center" class="font1"> <em><span class="font_judul"><strong>AGENCY FOR AGRICULTURAL QUARANTINE</strong></span></em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="center"><strong>KH - 1</strong></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><hr align="center" /></td>
          </tr>
        </table>
      </div></td>
    </tr>
    <tr>
      <td><div align="center">
        <table width="1000" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8"><div align="left">.</div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_judul"><div align="center"><u>PERMOHONAN PEMERIKSAAN KARANTINA HEWAN</u></div></td>
            <td><div align="left" class="diisi_petugas">
              <div align="center">Diisi Petugas</div>
            </div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_judul"><div align="center">APLICATION FOR ANIMAL QUARANTINE INSPECTION</div></td>
            <td><table width="70" border="1" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="70"><div align="left"></div></td>
            <td colspan="7" class="font_judul"><div align="center">Nomor : <?php echo $row_srt['no_sertifikat']; ?></div></td>
            <td width="23" class="font_judul"><div align="center"></div></td>
            <td width="70"><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="8"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"><u>Kepada Kepala/ Dokter Hewan Karantina</u></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"><em>To The Chief of Veterinary Animal Quarantine</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="left"><u>Pelabuhan Laut/ Udara*) di</u></div></td>
            <td colspan="5" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['daerah_asal']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="left"><em>Port/ Airport at</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Yang bertanda tangan dibawah ini</div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"><em>The Undersigned</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah"><div align="left">Nama</div></td>
            <td colspan="7" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['nm_pemohon']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"><em>Name</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah"><div align="left">Alamat</div></td>
            <td colspan="7" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['alamat']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"><em>Address</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="left">Identitas Pemohon</div></td>
            <td colspan="5" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['id_pemohon']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="left">Indentity (Owner)</div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Bertindak sebagai penerima/pengirim/pemilik*) dari hewan seperti disebut dibawah :</div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left">As the consignee/consignor/owner*) of the folowing animal (s) / animal product (s) / other product (s)*)</div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Yang akan dimuat ke/ dibongkar dari kapat laut/udara*)</div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"><em>Will be embarked to/ disembarked from*) the ship/ aircraft*)</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="8" class="font_isi_bawah"><div align="left"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="10"><div align="center">
              <table width="900" border="1" cellpadding="2" cellspacing="0">
                <tr>
                  <td><div align="center"><strong><span class="font_isi_bawah">No</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Jenis Hewan</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Tipe Hewan</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Jumlah</span></strong></div></td>
                  <td><div align="center"><strong><span class="font_isi_bawah">Uraian</span></strong></div></td>
                </tr>
                <?php do { ?>
                  <tr>
                    <td><div align="center"><span class="font_isi_bawah"><?php echo $i; ?></span></div></td>
                    <td class="font_isi_bawah"><?php echo $row_hwn['nama_brg']; ?></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['jenis_hewan']; ?></span></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['jumlah']; ?></span></td>
                    <td><span class="font_isi_bawah"><?php echo $row_hwn['uraian']; ?></span></td>
                  </tr> <?php $i++ ?>
                  <?php } while ($row_hwn = mysql_fetch_assoc($hwn)); ?>
              </table>
            </div>              
              <div align="center"></div>              <div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td width="90" class="font_isi_bawah"><div align="left"></div></td>
            <td width="162" class="font_isi_bawah">&nbsp;</td>
            <td colspan="4" class="font_isi_bawah">&nbsp;</td>
            <td width="107" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td width="180" class="font_isi_bawah"><div align="left"></div></td>
            <td width="196" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left">Alat angkut/ No              </div></td>
            <td colspan="4" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['alat_angkut']; ?></strong></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left"><em>Means of Transportation/Number</em> </div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="left">Negara/ Daerah Asal*)</div></td>
            <td colspan="4" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['daerah_asal']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td colspan="4" class="font_isi_bawah"><div align="left"><em>Country/Place of Origin*)</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="left">Negara/ Daerah Tujuan*)</div></td>
            <td colspan="4" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_srt_tgs['daerah_tujuan']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left"><em>Country/ Place of Destination*)</em></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="4" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="left">Perkiraan waktu Berangkat</div></td>
            <td colspan="4" rowspan="2" class="font_isi_bawah"><div align="left"><strong>: <?php echo $row_tgl_hwn['Tgl_hwn']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="left"><em>Estimate of departure</em></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="5" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
          </tr>
          <tr>
            <td><div align="left"></div></td>
            <td colspan="8" class="font_isi_bawah"><div align="left">Mohon diadakan pemeriksaan karantina terhadap hewan tersebut sesuai dengan peraturan yang berlaku.              </div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td colspan="8" class="font_isi_bawah"><div align="left"><em>Applying for the inspection of the above mentioned Animal(s) by your veterinarian</em> </div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="center"><?php echo $row_srt_tgs['daerah_asal']; ?> , <?php echo $row_tgl_brgkat['Tgl_brgkat']; ?></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td><div align="left">.</div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="left"></div></td>
            <td colspan="2" class="font_isi_bawah"><div align="left"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td><div align="left"></div></td>
            </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td colspan="4" class="font_isi_bawah"><div align="center"><strong><?php echo $row_srt_tgs['nm_pemohon']; ?></strong></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="3" class="font_isi_bawah"><div align="center"></div></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>.</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td colspan="2" class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td class="font_isi_bawah">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
mysql_free_result($srt);

mysql_free_result($hwn);

mysql_free_result($pemohon);

mysql_free_result($srt_tgs);

mysql_free_result($dokter);

mysql_free_result($tgl_klr);

mysql_free_result($tgl_hwn);

mysql_free_result($tgl_brgkat);
?>
