VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Entri_Surat_Tugas 
   Caption         =   "Data Surat Tugas"
   ClientHeight    =   7650
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10530
   LinkTopic       =   "Form2"
   ScaleHeight     =   7650
   ScaleWidth      =   10530
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame Frame3 
      Caption         =   "Filter"
      Height          =   975
      Left            =   0
      TabIndex        =   15
      Top             =   2880
      Width           =   9135
      Begin VB.CommandButton Cmdkeluar 
         Caption         =   "&Keluar"
         DragIcon        =   "surat_tugas.frx":0000
         Height          =   615
         Left            =   7920
         Picture         =   "surat_tugas.frx":0B02
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Cmdberikut 
         DownPicture     =   "surat_tugas.frx":1604
         Height          =   495
         Left            =   1680
         Picture         =   "surat_tugas.frx":2006
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdterakhir 
         Height          =   495
         Left            =   2400
         Picture         =   "surat_tugas.frx":2A08
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdpertama 
         Height          =   495
         Left            =   240
         Picture         =   "surat_tugas.frx":340A
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Cmdsebelum 
         Height          =   495
         Left            =   960
         Picture         =   "surat_tugas.frx":3E0C
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3855
      Left            =   9120
      TabIndex        =   9
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Cmdbatal 
         Caption         =   "&Batal"
         Height          =   615
         Left            =   120
         Picture         =   "surat_tugas.frx":480E
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   2400
         Width           =   975
      End
      Begin VB.CommandButton Cmdtambah 
         Caption         =   "&Tambah"
         Height          =   615
         Left            =   120
         Picture         =   "surat_tugas.frx":5210
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Cmdsimpan 
         Caption         =   "&Simpan"
         Height          =   615
         Left            =   120
         Picture         =   "surat_tugas.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton Cmdedit 
         Caption         =   "&Edit"
         Height          =   615
         Left            =   120
         Picture         =   "surat_tugas.frx":6614
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   975
      End
      Begin VB.CommandButton Cmdhapus 
         Caption         =   "&Hapus"
         Height          =   615
         Left            =   120
         Picture         =   "surat_tugas.frx":7016
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   3120
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Entri Surat Tugas"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9135
      Begin VB.TextBox txt_pelabuhan_muat 
         Height          =   375
         Left            =   6720
         TabIndex        =   30
         Top             =   360
         Width           =   2175
      End
      Begin VB.TextBox txt_keterangan 
         Height          =   735
         Left            =   6720
         MultiLine       =   -1  'True
         TabIndex        =   29
         Top             =   1800
         Width           =   2175
      End
      Begin VB.TextBox txt_alat_angkut 
         Height          =   375
         Left            =   6720
         TabIndex        =   28
         Top             =   1320
         Width           =   2175
      End
      Begin VB.TextBox txt_pelabuhan_bongkar 
         Height          =   375
         Left            =   6720
         TabIndex        =   25
         Top             =   840
         Width           =   2175
      End
      Begin VB.TextBox txt_daerah_tujuan 
         Height          =   375
         Left            =   2400
         TabIndex        =   23
         Top             =   1800
         Width           =   2175
      End
      Begin VB.TextBox txt_daerah_asal 
         Height          =   375
         Left            =   2400
         TabIndex        =   3
         Top             =   1320
         Width           =   2175
      End
      Begin VB.TextBox txt_id_pemohon 
         Height          =   375
         Left            =   2400
         TabIndex        =   2
         Top             =   840
         Width           =   2175
      End
      Begin VB.TextBox txt_no_srt 
         Height          =   375
         Left            =   2400
         TabIndex        =   1
         Top             =   360
         Width           =   2175
      End
      Begin MSComCtl2.DTPicker txt_tgl_surat_tgs 
         Bindings        =   "surat_tugas.frx":7A18
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "yyyy-MM-dd"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   3
         EndProperty
         Height          =   375
         Left            =   2400
         TabIndex        =   4
         Top             =   2280
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "yyyy-mm-dd"
         Format          =   65470467
         CurrentDate     =   41886
      End
      Begin VB.Label Label1 
         Caption         =   "Nomor Surat"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Label9 
         Caption         =   "Keterangan"
         Height          =   255
         Left            =   5040
         TabIndex        =   27
         Top             =   1920
         Width           =   1695
      End
      Begin VB.Label Label8 
         Caption         =   "Alat Angkut"
         Height          =   255
         Left            =   5040
         TabIndex        =   26
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label Label7 
         Caption         =   "Pelabuhan Bongkar"
         Height          =   255
         Left            =   5040
         TabIndex        =   24
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Daerah Tujuan"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1920
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Tanggal Surat Tugas"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   2400
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "Daerah Asal"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Identitas Pemohon"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "Pelabuhan Muat"
         Height          =   255
         Left            =   5040
         TabIndex        =   5
         Top             =   480
         Width           =   1335
      End
   End
   Begin MSAdodcLib.Adodc Ado_Surat 
      Height          =   375
      Left            =   0
      Top             =   7080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   2
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBString     =   "Provider=MSDASQL.1;Persist Security Info=False;User ID=root;Data Source=si_serkes_hewan"
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   "surat_tugas"
      Caption         =   "Data Surat Tugas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Bindings        =   "surat_tugas.frx":7A40
      Height          =   3615
      Left            =   0
      TabIndex        =   21
      Top             =   3960
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   6376
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Entri_Surat_Tugas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Baru As Boolean


Private Sub Cmdbatal_Click()
Tombol True, True, False, False, True
Ado_Surat.Recordset.Cancel
Kosong
End Sub

Private Sub Cmdberikut_Click()
'Menuju ke record berikutnya
Ado_Surat.Recordset.MoveNext
'Jika berada di record terakhir menuju ke record terakhir
If Ado_Surat.Recordset.EOF Then
Ado_Surat.Recordset.MoveLast
End If
End Sub

Private Sub Cmdedit_Click()
Tombol False, False, True, True, False
With Ado_Surat.Recordset
txt_no_srt = !no_srt
txt_id_pemohon = !id_pemohon
txt_daerah_asal = !daerah_asal
txt_daerah_tujuan = !daerah_tujuan
txt_pelabuhan_muat = !pelabuhan_muat
txt_pelabuhan_bongkar = !pelabuhan_bongkar
txt_alat_angkut = !alat_angkut
txt_keterangan = !keterangan
txt_tgl_surat_tgs = !tgl_surat_tgs
End With
txt_no_srt.SetFocus
Baru = False
End Sub

Private Sub Cmdhapus_Click()
Dim hapus
hapus = MsgBox("Apakah anda yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus Data")
If hapus = vbYes Then
    Ado_Surat.Recordset.Delete
    Ado_Surat.Recordset.MoveLast
Else
    MsgBox "Data tidak jadi dihapus!", vbOKOnly + vbInformation, "Batal menghapus"
End If
End Sub

Private Sub Cmdkeluar_Click()
Unload Me
End Sub

Private Sub Cmdpertama_Click()
'Menuju ke record pertama
Ado_Surat.Recordset.MoveFirst
End Sub

Private Sub Cmdsebelum_Click()
'Menuju ke record sebelumnya
Ado_Surat.Recordset.MovePrevious
'Jika berada di record pertama menuju ke record pertama
If Ado_Surat.Recordset.BOF Then
Ado_Surat.Recordset.MoveFirst
End If
End Sub

Private Sub Cmdsimpan_Click()
Tombol True, True, False, False, True
With Ado_Surat.Recordset
If Baru Then .AddNew
!no_srt = txt_no_srt.Text
!id_pemohon = txt_id_pemohon.Text
!daerah_asal = txt_daerah_asal.Text
!daerah_tujuan = txt_daerah_tujuan.Text
!pelabuhan_muat = txt_pelabuhan_muat.Text
!pelabuhan_bongkar = txt_pelabuhan_bongkar.Text
!alat_angkut = txt_alat_angkut.Text
!keterangan = txt_keterangan.Text
!tgl_surat_tgs = txt_tgl_surat_tgs.Value
.Update
.Sort = "no_srt"
End With
Kosong
End Sub

Private Sub Cmdtambah_Click()
Tombol False, False, True, True, False
Baru = True
Kosong
txt_no_srt.SetFocus
End Sub

Private Sub Cmdterakhir_Click()
Ado_Surat.Recordset.MoveLast
End Sub

Private Sub Form_Load()
Ado_Surat.ConnectionString = "DSN=si_serkes_hewan"
Ado_Surat.RecordSource = "surat_tugas"
Ado_Surat.Refresh
Ado_Surat.Recordset.Sort = "id_srt"
Baru = False
End Sub

Public Sub Tombol(tambah, edit, simpan, batal, hapus As Boolean)
Cmdtambah.Enabled = tambah
Cmdedit.Enabled = edit
Cmdsimpan.Enabled = simpan
Cmdbatal.Enabled = batal
Cmdhapus.Enabled = hapus
End Sub

Public Sub Kosong()
txt_no_srt.Text = ""
txt_id_pemohon.Text = ""
txt_daerah_asal.Text = ""
txt_daerah_tujuan.Text = ""
txt_pelabuhan_muat.Text = ""
txt_pelabuhan_bongkar.Text = ""
txt_alat_angkut.Text = ""
txt_keterangan.Text = ""
End Sub
